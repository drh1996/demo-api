-- mysql version 8.0.26
DROP DATABASE IF EXISTS `demo`;
DROP FUNCTION IF EXISTS `uuid_to_bin`;
DROP FUNCTION IF EXISTS `bin_to_uuid`;
DROP USER IF EXISTS 'demoapi'@'localhost';

CREATE DATABASE demo;

CREATE USER 'demoapi'@'localhost' IDENTIFIED BY 'dummyPassword';
GRANT ALL PRIVILEGES ON demo.* TO 'demoapi'@'localhost';

use demo;

DELIMITER $$
CREATE FUNCTION uuid_to_bin (uuid char(36))
RETURNS BINARY(16)
DETERMINISTIC
BEGIN
  DECLARE bin_uuid BINARY(16);
  SET bin_uuid = unhex(replace(uuid, '-', ''));
  RETURN bin_uuid;
END$$

CREATE FUNCTION bin_to_uuid (bin_uuid BINARY(16))
RETURNS char(36)
DETERMINISTIC
BEGIN
  DECLARE char_uuid char(36);
  SET char_uuid = hex(bin_uuid);
  SET char_uuid = substring(char_uuid, 0, 8) + '-'
      + substring(char_uuid, 8, 12) + '-'
      + substring(char_uuid, 12, 16) + '-'
      + substring(char_uuid, 16, 20) + '-'
      + substring(char_uuid, 20, 32);
  RETURN char_uuid;
END$$
DELIMITER ;

select 'Creating tables' as 'Debug message';
CREATE TABLE `user` (
  id BINARY(16) PRIMARY KEY,
  email VARCHAR(254) NOT NULL UNIQUE,
  username VARCHAR(64) NOT NULL UNIQUE,
  password VARCHAR(64) NOT NULL,
  first_name VARCHAR(64) NOT NULL,
  last_name VARCHAR(64) NOT NULL,
  nickname VARCHAR(20),
  date_of_birth DATE NOT NULL,
  created_ts TIMESTAMP NOT NULL,
  updated_ts TIMESTAMP
);

CREATE TABLE `session` (
  id BINARY(16) PRIMARY KEY,
  `date` DATE NOT NULL,
  comment TEXT
);

CREATE TABLE `user_session_entry` (
  session_id BINARY(16) NOT NULL,
  user_id BINARY(16) NOT NULL,
  in_for_pence INT NOT NULL,
  out_for_pence INT NOT NULL,
  food_cost_pence INT NOT NULL,
  paid_for_food BOOLEAN NOT NULL,

  PRIMARY KEY (session_id, user_id),
  FOREIGN KEY (session_id) REFERENCES `session`(id) ON DELETE CASCADE,
  FOREIGN KEY (user_id) REFERENCES `user`(id) ON DELETE CASCADE
);

CREATE TABLE `user_role` (
  id INT PRIMARY KEY AUTO_INCREMENT,
  user_id BINARY(16) NOT NULL,
  role VARCHAR(20) NOT NULL,

  FOREIGN KEY (user_id) REFERENCES `user`(id) ON DELETE CASCADE
);
CREATE INDEX user_role_index ON `user_role` (user_id);

select 'Inserting data' as 'Debug message';
INSERT INTO `user` (id, email, username, password, first_name, last_name, nickname, date_of_birth, created_ts, updated_ts) VALUES
(uuid_to_bin('145a3631-70eb-47f6-b70d-e6f83557f8da'), 'dan@email.com', 'Dan', '$2a$10$a5FFP/ykk4fbr/mY/9xBwOrjIktx8A06JcKdJ3sfrGhrSBQQPPRGW', 'Dan', 'Hayes', 'Dan', '1996-12-12', '2021-01-01 12:00:00', '2021-01-01 12:00:01'),
(uuid_to_bin('3fdd630b-59e9-4fc1-b54f-6b810e9905e2'), 'pierre@email.com', 'Pierre', '$2a$10$a5FFP/ykk4fbr/mY/9xBwOrjIktx8A06JcKdJ3sfrGhrSBQQPPRGW', 'Pierre', 'Jesus', 'Pie', '1996-12-12', '2021-01-01 12:00:00', '2021-01-01 12:00:01'),
(uuid_to_bin('27e444cc-49a2-466f-8950-7315743bdf26'), 'bernie@email.com', 'Bernie', '$2a$10$a5FFP/ykk4fbr/mY/9xBwOrjIktx8A06JcKdJ3sfrGhrSBQQPPRGW', 'Bernard', 'Abraham', 'Bernie', '1996-12-12', '2021-01-01 12:00:00', '2021-01-01 12:00:01');

INSERT INTO `session` (id, `date`, comment) VALUES
(uuid_to_bin('38f2e390-6d07-4a2c-b8fa-6105016a483d'), '2021-04-30', 'Example comment 1'),
(uuid_to_bin('eae506b8-7cae-4c9a-8786-9ba1a0976310'), '2021-06-04', 'Example comment 2'),
(uuid_to_bin('1735d5f5-34ee-456f-a2d9-2633a9da685f'), '2021-06-23', 'Example comment 3'),
(uuid_to_bin('4152e28b-3006-4e29-86e8-e4d991953c13'), '2021-07-21', 'Example comment 4'),
(uuid_to_bin('dab43eeb-57f6-4348-918c-8282f96391ae'), '2021-08-25', null);

INSERT INTO `user_session_entry` (session_id, user_id, in_for_pence, out_for_pence, food_cost_pence, paid_for_food) VALUES
(uuid_to_bin('38f2e390-6d07-4a2c-b8fa-6105016a483d'), uuid_to_bin('145a3631-70eb-47f6-b70d-e6f83557f8da'), 3000, 2460, 0, 0),
(uuid_to_bin('38f2e390-6d07-4a2c-b8fa-6105016a483d'), uuid_to_bin('3fdd630b-59e9-4fc1-b54f-6b810e9905e2'), 3000, 1350, 0, 0),
(uuid_to_bin('38f2e390-6d07-4a2c-b8fa-6105016a483d'), uuid_to_bin('27e444cc-49a2-466f-8950-7315743bdf26'), 1000, 3190, 0, 0),

(uuid_to_bin('4152e28b-3006-4e29-86e8-e4d991953c13'), uuid_to_bin('145a3631-70eb-47f6-b70d-e6f83557f8da'), 2000, 1240, 0, 1),
(uuid_to_bin('4152e28b-3006-4e29-86e8-e4d991953c13'), uuid_to_bin('3fdd630b-59e9-4fc1-b54f-6b810e9905e2'), 2000, 840, 1015, 0),
(uuid_to_bin('4152e28b-3006-4e29-86e8-e4d991953c13'), uuid_to_bin('27e444cc-49a2-466f-8950-7315743bdf26'), 1000, 2920, 0, 0),

(uuid_to_bin('1735d5f5-34ee-456f-a2d9-2633a9da685f'), uuid_to_bin('145a3631-70eb-47f6-b70d-e6f83557f8da'), 3000, 1650, 0, 1),
(uuid_to_bin('1735d5f5-34ee-456f-a2d9-2633a9da685f'), uuid_to_bin('3fdd630b-59e9-4fc1-b54f-6b810e9905e2'), 1000, 1260, 770, 0),
(uuid_to_bin('1735d5f5-34ee-456f-a2d9-2633a9da685f'), uuid_to_bin('27e444cc-49a2-466f-8950-7315743bdf26'), 3000, 4090, 500, 0),

(uuid_to_bin('dab43eeb-57f6-4348-918c-8282f96391ae'), uuid_to_bin('145a3631-70eb-47f6-b70d-e6f83557f8da'), 3000, 1555, 0, 1),
(uuid_to_bin('dab43eeb-57f6-4348-918c-8282f96391ae'), uuid_to_bin('3fdd630b-59e9-4fc1-b54f-6b810e9905e2'), 1000, 2880, 770, 0),
(uuid_to_bin('dab43eeb-57f6-4348-918c-8282f96391ae'), uuid_to_bin('27e444cc-49a2-466f-8950-7315743bdf26'), 3000, 2565, 900, 0),

(uuid_to_bin('eae506b8-7cae-4c9a-8786-9ba1a0976310'), uuid_to_bin('145a3631-70eb-47f6-b70d-e6f83557f8da'), 1000, 3340, 0, 0),
(uuid_to_bin('eae506b8-7cae-4c9a-8786-9ba1a0976310'), uuid_to_bin('3fdd630b-59e9-4fc1-b54f-6b810e9905e2'), 1000, 0, 0, 0),
(uuid_to_bin('eae506b8-7cae-4c9a-8786-9ba1a0976310'), uuid_to_bin('27e444cc-49a2-466f-8950-7315743bdf26'), 4000, 2660, 0, 0);

INSERT INTO `user_role` (id, user_id, role) VALUES
(1, uuid_to_bin('145a3631-70eb-47f6-b70d-e6f83557f8da'), 'ROLE_OWNER'),
(2, uuid_to_bin('145a3631-70eb-47f6-b70d-e6f83557f8da'), 'ROLE_USER'),
(3, uuid_to_bin('3fdd630b-59e9-4fc1-b54f-6b810e9905e2'), 'ROLE_USER'),
(4, uuid_to_bin('27e444cc-49a2-466f-8950-7315743bdf26'), 'ROLE_USER');