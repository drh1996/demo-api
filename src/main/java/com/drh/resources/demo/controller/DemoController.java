package com.drh.resources.demo.controller;

import static com.drh.resources.demo.utils.consts.DateFormats.DATE;

import com.drh.resources.demo.model.LoginRequest;
import com.drh.resources.demo.model.UserMatchTypeEnum;
import com.drh.resources.demo.persistance.dto.SessionDto;
import com.drh.resources.demo.persistance.dto.UpdateUserDto;
import com.drh.resources.demo.persistance.dto.UserDto;
import com.drh.resources.demo.service.DemoService;
import com.drh.resources.demo.service.jwt.model.JwtResponse;
import com.drh.resources.demo.utils.consts.Endpoints;
import com.drh.resources.demo.validation.factory.CustomLocalValidatorFactoryBean;
import com.drh.resources.demo.validation.validator.annotation.DateRange;
import com.drh.resources.demo.validation.validator.annotation.DateTimeFormat;
import com.drh.resources.demo.validation.validator.annotation.UserMatchType;
import com.drh.resources.demo.validation.validator.annotation.Username;
import com.drh.resources.demo.validation.validator.annotation.Uuid;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@Slf4j
@Validated
@RestController
@RequiredArgsConstructor
public class DemoController {

  private final DemoService demoService;
  private final CustomLocalValidatorFactoryBean validator;
  private final ModelMapper modelMapper;

  @PostMapping(value = Endpoints.LOGIN_USER,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public Mono<JwtResponse> loginUser(@Valid @RequestBody LoginRequest loginRequest) {
    log.info("Received request to login user");

    return demoService.loginUser(loginRequest);
  }

  @GetMapping(value = Endpoints.GET_USER, produces = MediaType.APPLICATION_JSON_VALUE)
  public UserDto getUser(
      @PathVariable String identifier,
      @RequestParam @UserMatchType String matchType) {
    log.info("Received request to get user by '{}' with value '{}'", matchType, identifier);

    validator.validateParam(UserDto.class, matchType.toLowerCase(), identifier, "identifier");

    return demoService.getUser(identifier, UserMatchTypeEnum.parseMatchType(matchType));
  }

  @PostMapping(value = Endpoints.CREATE_USER,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  public UserDto createUser(@Valid @RequestBody UserDto userDto) {
    log.info("Received request to create user with name '{}'", userDto.getUsername());
    return demoService.createUser(userDto);
  }

  @PatchMapping(value = Endpoints.UPDATE_USER,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public UserDto updateUser(
      @PathVariable @Username String username,
      @Valid @RequestBody UpdateUserDto userDto) {
    log.info("Received request to update user with username '{}'", username);
    return demoService.updateUser(username, userDto);
  }

  @GetMapping(value = Endpoints.SESSION, produces = MediaType.APPLICATION_JSON_VALUE)
  public SessionDto getSession(
      @RequestParam @DateRange @DateTimeFormat(format = DATE) String date) {
    log.info("Received request to get session");

    return demoService.getSession(modelMapper.map(date, LocalDate.class));
  }

  @GetMapping(value = Endpoints.SESSIONS, produces = MediaType.APPLICATION_JSON_VALUE)
  public List<SessionDto> getSessionsByDate(
      @RequestParam(required = false) @DateRange @DateTimeFormat(format = DATE) String startDate,
      @RequestParam(defaultValue = "0") @Min(value = 0) Integer offset,
      @RequestParam(defaultValue = "5") @Min(value = 5) @Max(value = 50) Integer limit) {

    LocalDate date;
    if (ObjectUtils.isEmpty(startDate)) { //use current date if one isn't provided
      date = LocalDate.now();
    } else {
      date = modelMapper.map(startDate, LocalDate.class);
    }

    log.info("Received request to find session that are on or before {}",
        startDate == null ? "today" : startDate);

    return demoService.getSessions(date, offset, limit);
  }

  @PostMapping(value = Endpoints.SESSION,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  public SessionDto createSession(@Valid @RequestBody SessionDto sessionDto) {
    log.info("Received request to create a session");
    return demoService.createSession(sessionDto);
  }

  @DeleteMapping(Endpoints.DELETE_SESSION)
  public void deleteSession(@PathVariable @Uuid String id) {
    log.info("Received request to delete session: {}", id);

    demoService.deleteSession(UUID.fromString(id));
  }
}
