package com.drh.resources.demo.exception;

import org.springframework.http.HttpStatus;

public class SessionNotFoundException extends DemoException {

  public SessionNotFoundException(String message, Object reference) {
    super(message, reference, HttpStatus.NOT_FOUND);
  }

}
