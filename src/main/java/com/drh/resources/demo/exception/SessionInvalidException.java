package com.drh.resources.demo.exception;

import org.springframework.http.HttpStatus;

public class SessionInvalidException extends DemoException {

  public SessionInvalidException(String message, Object reference) {
    super(message, reference, HttpStatus.BAD_REQUEST);
  }

}
