package com.drh.resources.demo.exception;

import org.springframework.http.HttpStatus;

public class EmailUnavailableException extends DemoException {

  public EmailUnavailableException(String message, Object user) {
    super(message, user, HttpStatus.BAD_REQUEST);
  }

}
