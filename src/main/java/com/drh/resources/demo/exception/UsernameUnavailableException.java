package com.drh.resources.demo.exception;

import org.springframework.http.HttpStatus;

public class UsernameUnavailableException extends DemoException {

  public UsernameUnavailableException(String message, Object user) {
    super(message, user, HttpStatus.BAD_REQUEST);
  }

}
