package com.drh.resources.demo.exception;

import org.springframework.http.HttpStatus;

public class UserNotFoundException extends DemoException {

  public UserNotFoundException(String message, Object user) {
    super(message, user, HttpStatus.NOT_FOUND);
  }
}
