package com.drh.resources.demo.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class DemoException extends RuntimeException {

  private final Object reference;
  private final HttpStatus status;

  public DemoException(String message, Object reference, HttpStatus status) {
    super(message);
    this.reference = reference;
    this.status = status;
  }
}
