package com.drh.resources.demo.exception;

import java.util.List;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;

@Getter
@RequiredArgsConstructor
public class ParameterValidationException extends RuntimeException {

  private final Object target;
  private final String paramName;
  private final List<FieldError> fieldErrors;
}
