package com.drh.resources.demo.exception;

import org.springframework.http.HttpStatus;

public class UserUnauthorizedException extends DemoException {

  public UserUnauthorizedException(String message, Object user) {
    super(message, user, HttpStatus.UNAUTHORIZED);
  }
}
