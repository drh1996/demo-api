package com.drh.resources.demo.model;

public enum UserMatchTypeEnum {
  USERNAME,
  EMAIL;

  public static UserMatchTypeEnum parseMatchType(String value) {
    for (UserMatchTypeEnum matchType : values()) {
      if (matchType.name().equalsIgnoreCase(value)) {
        return matchType;
      }
    }
    return null;
  }

}
