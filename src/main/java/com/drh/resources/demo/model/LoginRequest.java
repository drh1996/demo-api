package com.drh.resources.demo.model;

import com.drh.resources.demo.validation.validator.annotation.Username;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {

  @Username
  @NotNull(message = "error.validation.null.default")
  private String username;

  @NotNull(message = "error.validation.null.default")
  private String password;
}
