package com.drh.resources.demo.handler.model;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationErrorDetails {

  private String name;
  private List<String> messages;
  private Object provided;
}
