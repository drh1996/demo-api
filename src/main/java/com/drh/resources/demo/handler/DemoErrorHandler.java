package com.drh.resources.demo.handler;

import static java.util.Collections.singletonList;

import com.drh.resources.demo.exception.DemoException;
import com.drh.resources.demo.exception.ParameterValidationException;
import com.drh.resources.demo.handler.model.DemoError;
import com.drh.resources.demo.handler.model.ValidationError;
import com.drh.resources.demo.handler.model.ValidationErrorDetails;
import com.drh.resources.demo.utils.SpringUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

@Slf4j
@ControllerAdvice
@RequiredArgsConstructor
public class DemoErrorHandler {

  private final ReloadableResourceBundleMessageSource messageSource;

  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  //handles request body validation errors
  public ValidationError handleRequestBodyValidationException(MethodArgumentNotValidException e) {
    String targetClassName = "unknown";
    BindingResult result = e.getBindingResult();
    Object obj = e.getTarget();
    if (obj != null) {
      targetClassName = obj.getClass().getSimpleName();
    }

    log.error("Validation error caught for {}: {}", targetClassName, e.getMessage());

    List<ObjectError> allErrors = result.getAllErrors();
    Map<String, ValidationErrorDetails> errorsMap = new HashMap<>(allErrors.size());
    allErrors.forEach(error -> {
      FieldError fieldError = (FieldError) error;
      String fieldName = fieldError.getField();

      if (errorsMap.containsKey(fieldName)) {
        errorsMap.get(fieldName)
            .getMessages()
            .add(fieldError.getDefaultMessage());
      } else {
        errorsMap.put(fieldName, constructValidationErrorDetails(fieldError));
      }
    });

    ValidationError validationError = new ValidationError();
    validationError.setMessage(getMessage("error.validation.body.default"));
    validationError.setErrors(errorsMap.values());
    return validationError;
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(ParameterValidationException.class)
  //handles validateParam (manual) validation errors
  public ValidationError handleFieldValidationException(ParameterValidationException e) {
    String targetClassName = "unknown";
    List<FieldError> fieldErrors = e.getFieldErrors();
    Object obj = e.getTarget();
    if (obj != null) {
      targetClassName = obj.getClass().getSimpleName();
    }

    log.error("Validation error caught for {}: {}", targetClassName, e.getMessage());

    List<String> errorMessages = new ArrayList<>(fieldErrors.size());

    fieldErrors.forEach(fieldError -> errorMessages.add(fieldError.getDefaultMessage()));

    ValidationErrorDetails errorDetails = new ValidationErrorDetails();
    errorDetails.setName(e.getParamName());
    errorDetails.setProvided(obj);
    errorDetails.setMessages(errorMessages);

    ValidationError validationError = new ValidationError();
    validationError.setMessage(getMessage("error.validation.param.default"));
    validationError.setErrors(singletonList(errorDetails));
    return validationError;
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(ConstraintViolationException.class)
  //handles method level validation errors (query params, path variables, etc)
  public ValidationError handleConstrainViolation(ConstraintViolationException e) {
    log.error("Validation error caught for {}: {}", e.getClass().getSimpleName(), e.getMessage());

    Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
    Map<String, ValidationErrorDetails> errorsMap = new HashMap<>(violations.size());

    for (ConstraintViolation<?> v : violations) {
      String path = ((PathImpl) v.getPropertyPath()).getLeafNode().asString();
      if (errorsMap.containsKey(path)) {
        errorsMap.get(path)
            .getMessages()
            .add(v.getMessage());
      } else {
        List<String> messages = new ArrayList<>();
        messages.add(v.getMessage());

        ValidationErrorDetails details = new ValidationErrorDetails();
        details.setName(path);
        details.setMessages(messages);
        details.setProvided(v.getInvalidValue());
        errorsMap.put(path, details);
      }
    }

    ValidationError validationError = new ValidationError();
    validationError.setMessage(getMessage("error.validation.param.default"));
    validationError.setErrors(errorsMap.values());
    return validationError;
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(NoHandlerFoundException.class)
  public DemoError handleNoHandlerFound(NoHandlerFoundException e) {
    log.error("No handler found exception caught: {}", e.getMessage());

    return new DemoError(HttpStatus.NOT_FOUND.value(),
        HttpStatus.NOT_FOUND.getReasonPhrase(),
        getMessage("error.validation.endpoint.not-found"),
        e.getRequestURL());
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MissingServletRequestParameterException.class)
  public DemoError handleMissingQueryParam(MissingServletRequestParameterException e) {
    log.error("Missing query parameter exception caught: {}", e.getMessage());

    return new DemoError(HttpStatus.BAD_REQUEST.value(),
        HttpStatus.BAD_REQUEST.getReasonPhrase(),
        getMessage("error.validation.query-param.missing"),
        e.getParameterName());
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  public DemoError handleMethodNotSupported(HttpRequestMethodNotSupportedException e) {
    log.error("Method not supported exception caught: {}", e.getMessage());

    String supportedMethodsStr = "none";
    String[] supportedMethods = e.getSupportedMethods();
    if (supportedMethods != null) {
      supportedMethodsStr = Arrays.stream(supportedMethods)
          .sorted()
          .reduce((prev, next) -> prev + ", " + next)
          .orElse("none");
    }

    return new DemoError(HttpStatus.METHOD_NOT_ALLOWED.value(),
        HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase(),
        getMessage("error.validation.method.not-supported", e.getMethod()),
        getMessage("error.validation.method.supported", supportedMethodsStr));
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  public DemoError handleMediaTypeNotSupported(HttpMediaTypeNotSupportedException e) {
    log.error("Not supported media type exception caught: {}", e.getMessage());

    String mediaTypeString = "none";
    MediaType contentType = e.getContentType();
    if (contentType != null) {
      mediaTypeString = contentType.getType() + '/' + contentType.getSubtype();
    }

    return new DemoError(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(),
        HttpStatus.UNSUPPORTED_MEDIA_TYPE.getReasonPhrase(),
        getMessage("error.validation.media-type.not-supported"),
        mediaTypeString);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  public DemoError handleTypeMismatch(MethodArgumentTypeMismatchException e) {
    log.error("Argument type mismatch exception caught: {}", e.getMessage());

    Object value = e.getValue();
    String expected = e.getParameter().getParameterType().getSimpleName();
    String provided = value == null ? "null" : value.getClass().getSimpleName();

    return new DemoError(HttpStatus.BAD_REQUEST.value(),
        HttpStatus.BAD_REQUEST.getReasonPhrase(),
        getMessage("error.validation.method-args.mismatch", e.getName(), expected, provided),
        value);
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(HttpMessageNotReadableException.class)
  public DemoError handleMessageNotReadable(HttpMessageNotReadableException e) {
    log.error("http message not readable exception caught: {}", e.getMessage());

    String reason = "";

    Throwable cause = e.getCause();
    if (cause instanceof JsonParseException) {
      reason = getMessage("error.validation.body.invalid-json");
    } else if (cause instanceof MismatchedInputException) { //valid json but invalid root / field
      reason = handleMismatchedInputException((MismatchedInputException) cause);
    }

    return new DemoError(HttpStatus.BAD_REQUEST.value(),
        HttpStatus.BAD_REQUEST.getReasonPhrase(),
        getMessage("error.validation.body.parse-fail"),
        reason);
  }

  @ExceptionHandler(DemoException.class)
  public ResponseEntity<DemoError> handleDemoExceptions(DemoException e) {
    String message = getMessage(e.getMessage());
    log.error("{} caught with message: {}", e.getClass().getSimpleName(), message);
    return ResponseEntity
        .status(e.getStatus())
        .body(constructDemoError(e.getStatus(), message, e.getReference()));
  }

  @ResponseBody
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(Exception.class)
  public DemoError handleUnknownException(Exception e) {
    log.error("Unexpected {} caught with message: {}",
        e.getClass().getSimpleName(), e.getMessage());

    return new DemoError(HttpStatus.INTERNAL_SERVER_ERROR.value(),
        HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
        getMessage("error.unknown.message"),
        getMessage("error.unknown.more-info"));
  }

  private String getMessage(String code, Object... args) {
    return messageSource.getMessage(code, args, code, Locale.UK);
  }

  private static String handleMismatchedInputException(MismatchedInputException ex) {
    //parse json path
    String path = ex.getPath().stream()
        .map(reference -> SpringUtils.ifEmptyReturn(reference.getFieldName(),
            () -> "[" + reference.getIndex() + "]"))
        .reduce((prev, next) -> {
          if (next.matches("\\[\\d+]")) {
            return prev + next;
          } else {
            return prev + '.' + next;
          }
        }).orElse(null);

    //parse target field type
    Class<?> targetType = ex.getTargetType();
    String targetTypeStr = targetType.getSimpleName();
    if (targetType.isArray() || targetType.isAssignableFrom(Collection.class)) {
      targetTypeStr = "Array";
    } else if (String.class.equals(targetType)) {
      targetTypeStr = "String";
    } else if (!targetType.isPrimitive()) {
      targetTypeStr = "Object";
    }

    String reason = (path != null ? "field '" + path + '\'' : "root")
        + " expected " + targetTypeStr;
    if (ex instanceof InvalidFormatException) {
      //parse received field
      Object value = ((InvalidFormatException) ex).getValue();
      reason += " but received ";
      if (value != null) {
        reason += '\'' + value.toString() + "' (" + value.getClass().getSimpleName() + ')';
      } else {
        reason += "null";
      }
    }
    return reason;
  }

  private static DemoError constructDemoError(HttpStatus status, String message, Object moreInfo) {
    return new DemoError(status.value(), status.getReasonPhrase(), message, moreInfo);
  }

  private static ValidationErrorDetails constructValidationErrorDetails(FieldError error) {
    List<String> messages = new ArrayList<>();
    messages.add(error.getDefaultMessage());

    ValidationErrorDetails details = new ValidationErrorDetails();
    details.setName(error.getField());
    details.setMessages(messages);
    details.setProvided(error.getRejectedValue());
    return details;
  }
}
