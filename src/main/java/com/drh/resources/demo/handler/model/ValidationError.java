package com.drh.resources.demo.handler.model;

import java.util.Collection;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationError {

  private String message;
  private Collection<ValidationErrorDetails> errors;
}
