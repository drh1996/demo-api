package com.drh.resources.demo.handler.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class DemoError {

  private final int httpStatus;
  private final String error;
  private final String message;

  @JsonInclude(Include.NON_NULL)
  private final Object moreInformation;

}
