package com.drh.resources.demo.utils.consts;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class RegexPatterns {

  public static final String USERNAME = "[a-zA-z0-9_]+";
  public static final String NAME = "[a-zA-Z ,.'-]+";
  public static final String TEXT = "[\\w!\"£$%^&*()\\-+=\\[\\]{}<>'@#~\\/\\?\\.,\\\\|\\s`¬]*";
}
