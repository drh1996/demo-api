package com.drh.resources.demo.utils.consts;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DateFormats {

  public static final String DATE = "dd/MM/yyyy";
}
