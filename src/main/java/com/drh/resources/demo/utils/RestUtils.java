package com.drh.resources.demo.utils;

import com.drh.resources.demo.exception.DemoException;
import io.netty.handler.timeout.ReadTimeoutException;
import io.netty.handler.timeout.WriteTimeoutException;
import java.net.ConnectException;
import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

@Component
@RequiredArgsConstructor
public class RestUtils {

  private final ReloadableResourceBundleMessageSource messageSource;

  public Throwable handleRestError(Throwable t) {
    HttpStatus responseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    String messageCode = "error.jwt.unknown";
    Object[] messageArgs = null;
    Object exceptionReference = null;

    if (t instanceof WebClientResponseException) {
      WebClientResponseException responseException = (WebClientResponseException) t;

      responseStatus = responseException.getStatusCode();
      messageCode = "error.jwt.response-error";
      messageArgs = new Object[] { responseStatus.getReasonPhrase() };
      exceptionReference = responseException.getResponseBodyAsString();

    } else if (t instanceof WebClientRequestException) {
      Throwable rootCause = rootCause(t);
      if (rootCause instanceof ConnectException) {
        responseStatus = HttpStatus.SERVICE_UNAVAILABLE;
        messageCode = "error.jwt.connect-refused";
      } else if (rootCause instanceof ReadTimeoutException) {
        messageCode = "error.jwt.read-timeout";
      } else if (rootCause instanceof WriteTimeoutException) {
        messageCode = "error.jwt.write-timeout";
      } else {
        messageCode = "error.jwt.request";
      }
    }

    return new DemoException(
        messageSource.getMessage(messageCode, messageArgs, Locale.UK),
        exceptionReference,
        responseStatus);
  }

  private static Throwable rootCause(Throwable t) {
    Throwable cause = t;
    while (cause.getCause() != null) {
      cause = cause.getCause();
    }
    return cause;
  }

}
