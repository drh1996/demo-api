package com.drh.resources.demo.utils;

import java.util.function.Consumer;
import java.util.function.Function;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.ObjectUtils;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SpringUtils {

  public static <T> void ifNotEmpty(T obj, Consumer<T> callbackFunc) {
    if (!ObjectUtils.isEmpty(obj)) {
      callbackFunc.accept(obj);
    }
  }

  public static <R> R ifEmptyReturn(R obj, Method<R> callbackFunc) {
    return ObjectUtils.isEmpty(obj) ? callbackFunc.call() : obj;
  }

  public static <R> R ifNullReturn(R obj, Method<R> callbackFunc) {
    return obj == null ? callbackFunc.call() : obj;
  }

  @FunctionalInterface
  public interface Method<R> {
    R call();
  }
}
