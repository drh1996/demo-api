package com.drh.resources.demo.utils.consts;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Endpoints {
  public static final String LOGIN_USER = "/user/login";
  public static final String GET_USER = "/user/{identifier}";
  public static final String CREATE_USER = "/user";
  public static final String UPDATE_USER = "/user/{username}";

  public static final String SESSION = "/session";
  public static final String SESSIONS = "/sessions";
  public static final String DELETE_SESSION = "/session/{id}";
}
