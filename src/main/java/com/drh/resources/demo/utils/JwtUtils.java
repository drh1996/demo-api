package com.drh.resources.demo.utils;

import com.drh.resources.demo.exception.JwtAuthenticationException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JwtUtils {

  private static final String BEARER = "Bearer ";

  public static boolean validateToken(String token, String secret, UserDetails userDetails) {
    final String username = getUsernameFromToken(token, secret);
    return username.equals(userDetails.getUsername())
        && !isTokenExpired(token, secret)
        && isValidRoles(token, secret, userDetails);
  }

  public static String getUsernameFromToken(String token, String secret) {
    return getClaimFromToken(token, secret, Claims::getSubject);
  }

  private static <T> T getClaimFromToken(
      String token,
      String secret,
      Function<Claims, T> claimsResolver) {

    return claimsResolver.apply(Jwts.parser()
        .setSigningKey(secret.getBytes(StandardCharsets.UTF_8))
        .parseClaimsJws(token)
        .getBody());
  }

  private static boolean isTokenExpired(String token, String secret) {
    final Date expiration = getClaimFromToken(token, secret, Claims::getExpiration);
    if (expiration == null) {
      return false;
    }
    return expiration.before(new Date());
  }

  @SuppressWarnings("unchecked")
  private static boolean isValidRoles(String token, String secret, UserDetails userDetails) {
    Object rolesObj = getClaimFromToken(token, secret, claims -> claims.get("roles"));
    if (rolesObj != null && Collection.class.isAssignableFrom(rolesObj.getClass())) {
      Set<Object> roles = new HashSet<>((Collection<Object>) rolesObj);
      return userDetails.getAuthorities()
          .stream()
          .map(GrantedAuthority::getAuthority)
          .collect(Collectors.toSet())
          .equals(roles);
    }
    return false;
  }

  public static String parseToken(String token) {
    if (token != null && token.startsWith(BEARER)) {
      return token.substring(BEARER.length());
    }
    throw new BadCredentialsException("Bearer is missing");
  }

  public static JwtAuthenticationException readJwtError(Exception e) {
    log.info("An error occurred validating the JWT, {}: {}",
        e.getClass().getSimpleName(), e.getMessage());

    String message = e.getMessage();
    if (e instanceof IllegalArgumentException) {
      message = "JWT contains illegal arguments.";
    } else if (e instanceof ExpiredJwtException) {
      message = "Expired JWT provided.";
    } else if (e instanceof SignatureException) {
      message = "JWT contents doesn't match it's signature.";
    } else if (e instanceof MalformedJwtException) {
      message = "JWT is malformed.";
    }
    //} else if (e instanceof JwtNotFoundException) {
    //  message = "JWT is not present or doesn't start with 'Bearer '";
    //}

    return new JwtAuthenticationException(message, e);
  }
}
