package com.drh.resources.demo.configuration.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PassayConfig {

  private int minLength;
  private int maxLength;
  private int upperCaseCount;
  private int lowerCaseCount;
  private int digitCount;
  private int specialCount;
}
