package com.drh.resources.demo.configuration.security.model;

public enum Roles {
  USER,
  MAINTAINER,
  OWNER
}
