package com.drh.resources.demo.configuration.security.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

@Slf4j
public class DemoAccessDeniedHandler implements AccessDeniedHandler {

  private static final byte[] ERROR_RESPONSE = createErrorResponse();

  @Override
  public void handle(
      HttpServletRequest request,
      HttpServletResponse response,
      AccessDeniedException accessDeniedException) throws IOException {

    log.debug("Creating forbidden error response for: {}", accessDeniedException.getMessage());

    response.setContentType("application/json");
    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    response.getOutputStream().write(ERROR_RESPONSE);
  }

  private static byte[] createErrorResponse() {
    return new ObjectMapper().createObjectNode()
        .put("httpStatus", HttpStatus.FORBIDDEN.value())
        .put("error", HttpStatus.FORBIDDEN.getReasonPhrase())
        .put("message", "Access is denied")
        .toString().getBytes(StandardCharsets.UTF_8);
  }
}
