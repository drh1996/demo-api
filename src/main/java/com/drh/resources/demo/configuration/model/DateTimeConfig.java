package com.drh.resources.demo.configuration.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DateTimeConfig {

  private String dateFormat;
  private String dateTimeFormat;
}
