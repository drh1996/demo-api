package com.drh.resources.demo.configuration.modelmapper;

import com.drh.resources.demo.persistance.dao.SessionDao;
import com.drh.resources.demo.persistance.dao.UserDao;
import com.drh.resources.demo.persistance.dao.UserSessionEntryDao;
import com.drh.resources.demo.persistance.dto.Nullable;
import com.drh.resources.demo.persistance.dto.SessionDto;
import com.drh.resources.demo.persistance.dto.UpdateUserDto;
import com.drh.resources.demo.persistance.dto.UserData;
import com.drh.resources.demo.persistance.dto.UserDto;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.Condition;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

public final class ModelMapperCreator {

  private static final Condition<?, ?> nonNull = c -> Objects.nonNull(c.getSource());

  private final DateTimeFormatter dateFormat;
  private final DateTimeFormatter dateTimeFormat;

  private Function<String, LocalDate> dateFunc;
  private Function<String, LocalDateTime> dateTimeFunc;

  private Converter<LocalDate, String> dateStringConvert;
  private Converter<String, LocalDate> stringDateConvert;
  private Converter<LocalDateTime, String> dateTimeStringConvert;
  private Converter<String, LocalDateTime> stringDateTimeConvert;
  private Converter<Nullable<?>, ?> nullableConvert;

  public ModelMapperCreator(DateTimeFormatter dateFormat, DateTimeFormatter dateTimeFormat) {
    this.dateFormat = dateFormat;
    this.dateTimeFormat = dateTimeFormat;
    setupFunctions();
    setupCommonConverters();
  }

  private void setupFunctions() {
    dateFunc = src -> LocalDate.parse(src, dateFormat);
    dateTimeFunc = src -> LocalDateTime.parse(src, dateTimeFormat);
  }

  private void setupCommonConverters() {
    //LocalDate converters
    dateStringConvert = c -> dateFormat.format(c.getSource());
    stringDateConvert = c -> dateFunc.apply(c.getSource());

    //LocalDateTime converters
    dateTimeStringConvert = c -> dateTimeFormat.format(c.getSource());
    stringDateTimeConvert = c -> dateTimeFunc.apply(c.getSource());

    //Nullable converters
    nullableConvert = c -> c.getSource().get();
  }

  public ModelMapper create() {
    //UserSessionEntryDao converters
    Converter<Set<UserSessionEntryDao>, List<String>> userIdConvert = c ->
        c.getSource().stream()
            .map(e -> e.getSession().getId().toString())
            .collect(Collectors.toList());

    Converter<Set<UserSessionEntryDao>, List<UserData>> userDataConverter = c ->
        c.getSource().stream()
            .map(entry -> {
              UserData userData = new UserData();
              userData.setUsername(entry.getUser().getUsername());
              userData.setInForPence(entry.getInForPence());
              userData.setOutForPence(entry.getOutForPence());
              userData.setFoodCostPence(entry.getFoodCostPence());
              userData.setPaidForFood(entry.isPaidForFood());
              return userData;
            })
            .collect(Collectors.toList());

    ModelMapper mm = new ModelMapper();

    mm.addConverter(dateTimeStringConvert);
    mm.addConverter(stringDateTimeConvert);
    mm.addConverter(dateStringConvert);
    mm.addConverter(stringDateConvert);
    mm.addConverter(nullableConvert);
    mm.addConverter(userIdConvert);

    mm.addMappings(new UserDaoToDtoPropertyMap(userIdConvert));
    mm.addMappings(new UserDtoToDaoPropertyMap());
    mm.addMappings(new UpdateUserDtoToUserDaoPropertyMap());
    mm.addMappings(new SessionDaoToDtoPropertyMap(userDataConverter));
    mm.addMappings(new SessionDtoToDaoPropertyMap());

    //for calls like modelMapper.map("12/12/1996", LocalDate.class)
    mm.createTypeMap(String.class, LocalDate.class)
        .setProvider(request -> dateFunc.apply((String) request.getSource()));

    mm.createTypeMap(LocalDate.class, String.class)
        .setPreConverter(dateStringConvert);

    mm.validate();
    return mm;
  }

  //UserDao -> UserDto
  @RequiredArgsConstructor
  private class UserDaoToDtoPropertyMap extends PropertyMap<UserDao, UserDto> {
    private final Converter<Set<UserSessionEntryDao>, List<String>> userIdConvert;

    @Override
    protected void configure() {
      //never get fields
      skip().setPassword(null);
      //setters are ignored they act as placeholders
      map(source.getId()).setUserId(null);
      map(source.getEmail()).setEmail(null);
      map(source.getUsername()).setUsername(null);
      map(source.getFirstName()).setFirstName(null);
      map(source.getLastName()).setLastName(null);
      map(source.getNickname()).setNickname(null);

      when(nonNull).using(userIdConvert).map(source.getUserSessionEntries()).setSessions(null);
      when(nonNull).using(dateStringConvert).map(source.getDateOfBirth()).setDob(null);
      when(nonNull).using(dateTimeStringConvert).map(source.getCreatedTs()).setCreatedTs(null);
      when(nonNull).using(dateTimeStringConvert).map(source.getUpdatedTs()).setUpdatedTs(null);
    }
  }

  //UserDto -> UserDao
  private class UserDtoToDaoPropertyMap extends PropertyMap<UserDto, UserDao> {
    @Override
    protected void configure() {
      //never set fields
      skip().setId(null);
      skip().setUserSessionEntries(null);
      skip().setUserRoles(null);

      //setters are ignored they act as placeholders
      map(source.getEmail()).setEmail(null);
      map(source.getUsername()).setUsername(null);
      map(source.getPassword()).setPassword(null);
      map(source.getFirstName()).setFirstName(null);
      map(source.getLastName()).setLastName(null);
      map(source.getNickname()).setNickname(null);

      when(nonNull).using(stringDateConvert).map(source.getDob()).setDateOfBirth(null);
      when(nonNull).using(stringDateTimeConvert).map(source.getCreatedTs()).setCreatedTs(null);
      when(nonNull).using(stringDateTimeConvert).map(source.getUpdatedTs()).setUpdatedTs(null);
    }
  }

  //UpdateUserDto -> UserDao property map
  private class UpdateUserDtoToUserDaoPropertyMap extends PropertyMap<UpdateUserDto, UserDao> {
    @Override
    protected void configure() {
      //never set fields
      skip().setId(null);
      skip().setCreatedTs(null);
      skip().setUpdatedTs(null);
      skip().setUserSessionEntries(null);
      skip().setUserRoles(null);

      //setters are ignored they act as placeholders
      //when value is not null, set using corresponding setter
      when(nonNull).map(source.getEmail()).setEmail(null);
      when(nonNull).map(source.getUsername()).setUsername(null);
      when(nonNull).map(source.getPassword()).setPassword(null);
      when(nonNull).map(source.getFirstName()).setFirstName(null);
      when(nonNull).map(source.getLastName()).setLastName(null);
      when(nonNull).using(stringDateConvert).map(source.getDob()).setDateOfBirth(null);

      //when Nullable is not null, map wrapped value to corresponding setter
      when(nonNull).using(nullableConvert).map(source.getNickname()).setNickname(null);
    }
  }

  //SessionDao -> SessionDto
  @RequiredArgsConstructor
  private class SessionDaoToDtoPropertyMap extends PropertyMap<SessionDao, SessionDto> {
    private final Converter<Set<UserSessionEntryDao>, List<UserData>> userDataConvert;

    @Override
    protected void configure() {
      map(source.getId()).setId(null);
      map(source.getComment()).setComment(null);
      when(nonNull).using(dateStringConvert).map(source.getDate()).setDate(null);
      when(nonNull).using(userDataConvert).map(source.getUserSessionEntries()).setUsers(null);
    }
  }

  //SessionDto -> SessionDao
  private class SessionDtoToDaoPropertyMap extends PropertyMap<SessionDto, SessionDao> {
    @Override
    protected void configure() {
      skip().setUserSessionEntries(null);

      map(source.getId()).setId(null);
      map(source.getComment()).setComment(null);
      when(nonNull).using(stringDateConvert).map(source.getDate()).setDate(null);
    }
  }
}
