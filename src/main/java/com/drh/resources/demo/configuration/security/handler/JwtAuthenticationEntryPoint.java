package com.drh.resources.demo.configuration.security.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

  private static final byte[] ERROR_RESPONSE = createErrorResponse();

  @Override
  public void commence(
      HttpServletRequest request,
      HttpServletResponse response,
      AuthenticationException authException) throws IOException {

    log.debug("Creating unauthorized error response for: {}", authException.getMessage());

    response.setContentType("application/json");
    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    response.getOutputStream().write(ERROR_RESPONSE);
  }

  private static byte[] createErrorResponse() {
    return new ObjectMapper().createObjectNode()
        .put("httpStatus", HttpStatus.UNAUTHORIZED.value())
        .put("error", HttpStatus.UNAUTHORIZED.getReasonPhrase())
        .put("message", "Unable to authorize request")
        .toString().getBytes(StandardCharsets.UTF_8);
  }
}
