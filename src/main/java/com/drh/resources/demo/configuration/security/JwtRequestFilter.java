package com.drh.resources.demo.configuration.security;

import com.drh.resources.demo.configuration.security.handler.JwtAuthenticationEntryPoint;
import com.drh.resources.demo.utils.JwtUtils;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

@Slf4j
@RequiredArgsConstructor
public class JwtRequestFilter extends OncePerRequestFilter {

  private final UserDetailsService jwtUserDetailsService;
  private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
  private final String secret;

  @Override
  protected void doFilterInternal(
      HttpServletRequest request,
      @NonNull HttpServletResponse response,
      @NonNull FilterChain chain) throws ServletException, IOException {

    final String requestTokenHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

    if (requestTokenHeader == null) {
      log.debug("No authorization provided, continuing to anonymous authentication.");
      chain.doFilter(request, response);
      return;
    }

    String jwtToken = null;
    String username = null;
    Exception caughtException = null;

    try {
      jwtToken = JwtUtils.parseToken(requestTokenHeader);
      username = JwtUtils.getUsernameFromToken(jwtToken, secret);
    } catch (Exception e) {
      log.debug("caught exception getting username from JWT, "
          + "continuing to validation/anonymous authentication. message: {}", e.getMessage());
      caughtException = e;
    }

    // Once we get the token validate it.
    if (jwtToken != null && username != null
        && SecurityContextHolder.getContext().getAuthentication() == null) {

      UserDetails userDetails = this.jwtUserDetailsService.loadUserByUsername(username);

      // if token is valid configure Spring Security to manually set authentication
      if (JwtUtils.validateToken(jwtToken, secret, userDetails)) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
            new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());

        usernamePasswordAuthenticationToken
            .setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

        SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
      }
    } else if (caughtException != null) {
      log.error("JWT is invalid: {}", caughtException.getMessage());
      jwtAuthenticationEntryPoint.commence(
          request, response, JwtUtils.readJwtError(caughtException));
      return;
    }

    chain.doFilter(request, response);
  }
}
