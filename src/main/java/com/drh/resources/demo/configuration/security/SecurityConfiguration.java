package com.drh.resources.demo.configuration.security;

import com.drh.resources.demo.configuration.security.handler.DemoAccessDeniedHandler;
import com.drh.resources.demo.configuration.security.handler.JwtAuthenticationEntryPoint;
import com.drh.resources.demo.configuration.security.model.Roles;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@RequiredArgsConstructor
@ConfigurationProperties("demo-security")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  private final UserDetailsService userDetailsService;
  private final PasswordEncoder passwordEncoder;
  private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

  @Setter
  private String secret;

  @Setter
  private String[] pathExclusions;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService)
        .passwordEncoder(passwordEncoder);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.addFilterBefore(jwtRequestFilter(), UsernamePasswordAuthenticationFilter.class)
        .csrf().disable()
        .authorizeRequests().antMatchers(pathExclusions).permitAll()
        .antMatchers(HttpMethod.GET).hasRole(Roles.USER.name())
        .antMatchers(HttpMethod.DELETE).hasAnyRole(
            Roles.MAINTAINER.name(), Roles.OWNER.name())
        .antMatchers(HttpMethod.POST).hasAnyRole(
            Roles.MAINTAINER.name(), Roles.OWNER.name())
        .antMatchers(HttpMethod.PATCH).hasAnyRole(
            Roles.MAINTAINER.name(), Roles.OWNER.name())
        .anyRequest().authenticated().and()
        .exceptionHandling().accessDeniedHandler(new DemoAccessDeniedHandler()).and()
        .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
  }

  @Bean
  public JwtRequestFilter jwtRequestFilter() {
    return new JwtRequestFilter(userDetailsService, jwtAuthenticationEntryPoint, secret);
  }
}
