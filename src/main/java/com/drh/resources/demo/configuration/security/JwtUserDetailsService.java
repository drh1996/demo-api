package com.drh.resources.demo.configuration.security;

import com.drh.resources.demo.configuration.security.model.JwtUserDetails;
import com.drh.resources.demo.persistance.dao.UserDao;
import com.drh.resources.demo.persistance.dao.UserRoleDao;
import com.drh.resources.demo.persistance.repository.UserRepository;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

  private final UserRepository userRepository;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    UserDao userDao = userRepository.findById(UUID.fromString(username))
        .orElseThrow(() -> new UsernameNotFoundException("User " + username + " not found"));

    return new JwtUserDetails(username, userDao.getPassword(), true,
        createAuthorities(userDao.getUserRoles()));
  }

  private static List<GrantedAuthority> createAuthorities(Set<UserRoleDao> roles) {
    return roles.stream()
        .map(userRole -> new SimpleGrantedAuthority(userRole.getRole()))
        .collect(Collectors.toList());
  }
}
