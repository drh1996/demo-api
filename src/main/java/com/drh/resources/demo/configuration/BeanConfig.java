package com.drh.resources.demo.configuration;

import com.drh.resources.demo.configuration.model.DateTimeConfig;
import com.drh.resources.demo.configuration.model.PassayConfig;
import com.drh.resources.demo.configuration.modelmapper.ModelMapperCreator;
import com.drh.resources.demo.validation.configuration.MessageSourceMessageInterpolator;
import com.drh.resources.demo.validation.factory.CustomLocalValidatorFactoryBean;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.validation.MessageInterpolator;
import lombok.Setter;
import org.modelmapper.ModelMapper;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.LengthRule;
import org.passay.MessageResolver;
import org.passay.PasswordValidator;
import org.passay.WhitespaceRule;
import org.passay.spring.SpringMessageResolver;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@ConfigurationProperties("bean-config")
public class BeanConfig {

  @Setter
  private DateTimeConfig dateTime;

  @Setter
  private PassayConfig passay;

  private DateTimeFormatter dateFormat;
  private DateTimeFormatter dateTimeFormat;

  @PostConstruct
  public void postConstruct() {
    Objects.requireNonNull(passay,
        "bean-config.passay missing from application.yml/.properties");
    Objects.requireNonNull(dateTime,
        "bean-config.date-time missing from application.yml/.properties");

    dateFormat = DateTimeFormatter.ofPattern(
        Objects.requireNonNull(dateTime.getDateFormat(), "date-format"));
    dateTimeFormat = DateTimeFormatter.ofPattern(
        Objects.requireNonNull(dateTime.getDateTimeFormat(), "date-time-format"));
  }

  @Bean
  public ModelMapper modelMapper() {
    return new ModelMapperCreator(dateFormat, dateTimeFormat).create();
  }

  @Bean
  public ObjectMapper objectMapper() {
    ObjectMapper om = new ObjectMapper();
    om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    SimpleModule module = new SimpleModule("demo-module");
    module.addSerializer(LocalDate.class, new JsonSerializer<>() {
      @Override
      public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider serializers)
          throws IOException {
        gen.writeString(dateFormat.format(value));
      }
    });

    om.registerModule(module);
    return om;
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Bean
  public ReloadableResourceBundleMessageSource messageSource() {
    ReloadableResourceBundleMessageSource messageSource =
        new ReloadableResourceBundleMessageSource();
    messageSource.setBasename("classpath:/locale/messages");
    messageSource.setDefaultEncoding("UTF-8");
    return messageSource;
  }

  @Bean
  public MessageInterpolator messageInterpolator(
      ReloadableResourceBundleMessageSource messageSource) {
    return new MessageSourceMessageInterpolator(messageSource);
  }

  @Bean
  public CustomLocalValidatorFactoryBean validator(MessageInterpolator messageInterpolator) {
    CustomLocalValidatorFactoryBean validator = new CustomLocalValidatorFactoryBean();
    validator.setMessageInterpolator(messageInterpolator);
    return validator;
  }

  @Bean
  public MessageResolver messageResolver(ReloadableResourceBundleMessageSource messageSource) {
    return new SpringMessageResolver(messageSource);
  }

  @Bean
  public PasswordValidator passwordValidator(MessageResolver messageResolver) {
    return new PasswordValidator(
        messageResolver,
        new LengthRule(passay.getMinLength(), passay.getMaxLength()),
        new CharacterRule(EnglishCharacterData.UpperCase, passay.getUpperCaseCount()),
        new CharacterRule(EnglishCharacterData.LowerCase, passay.getLowerCaseCount()),
        new CharacterRule(EnglishCharacterData.Digit, passay.getDigitCount()),
        new CharacterRule(EnglishCharacterData.Special, passay.getSpecialCount()),
        new WhitespaceRule());
  }
}
