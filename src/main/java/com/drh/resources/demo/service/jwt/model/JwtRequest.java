package com.drh.resources.demo.service.jwt.model;

import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtRequest {

  private String userId;
  private Long expiration;
  private String secret;
  private Map<String, Object> claims;
}
