package com.drh.resources.demo.service;

import com.drh.resources.demo.model.LoginRequest;
import com.drh.resources.demo.model.UserMatchTypeEnum;
import com.drh.resources.demo.persistance.dto.SessionDto;
import com.drh.resources.demo.persistance.dto.UpdateUserDto;
import com.drh.resources.demo.persistance.dto.UserDto;
import com.drh.resources.demo.service.jwt.model.JwtResponse;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import reactor.core.publisher.Mono;

public interface DemoService {

  Mono<JwtResponse> loginUser(LoginRequest loginRequest);

  UserDto getUser(String identifier, UserMatchTypeEnum matchType);

  UserDto createUser(UserDto userDto);

  UserDto updateUser(String name, UpdateUserDto userDto);

  SessionDto getSession(LocalDate date);

  List<SessionDto> getSessions(LocalDate date, int offset, int limit);

  SessionDto createSession(SessionDto sessionDto);

  void deleteSession(UUID uuid);
}
