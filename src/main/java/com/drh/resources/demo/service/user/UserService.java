package com.drh.resources.demo.service.user;

import com.drh.resources.demo.model.UserMatchTypeEnum;
import com.drh.resources.demo.persistance.dao.UserDao;
import com.drh.resources.demo.persistance.dto.UpdateUserDto;
import com.drh.resources.demo.persistance.dto.UserDto;
import java.util.Optional;

public interface UserService {

  UserDto getUser(String identifier, UserMatchTypeEnum matchType);

  Optional<UserDao> getUserDaoByUsername(String username);

  UserDto createUser(UserDto userDto);

  UserDto updateUser(String name, UpdateUserDto updateUserDto);


}
