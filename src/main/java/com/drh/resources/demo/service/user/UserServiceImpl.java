package com.drh.resources.demo.service.user;

import com.drh.resources.demo.exception.EmailUnavailableException;
import com.drh.resources.demo.exception.UserNotFoundException;
import com.drh.resources.demo.exception.UsernameUnavailableException;
import com.drh.resources.demo.model.UserMatchTypeEnum;
import com.drh.resources.demo.persistance.dao.UserDao;
import com.drh.resources.demo.persistance.dao.UserRoleDao;
import com.drh.resources.demo.persistance.dto.UpdateUserDto;
import com.drh.resources.demo.persistance.dto.UserDto;
import com.drh.resources.demo.persistance.repository.UserRepository;
import com.drh.resources.demo.persistance.repository.UserRoleRepository;
import com.drh.resources.demo.utils.SpringUtils;
import java.util.Map;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;
  private final UserRoleRepository userRoleRepository;
  private final ModelMapper modelMapper;

  @Override
  public UserDto getUser(String identifier, UserMatchTypeEnum matchType) {
    Optional<UserDao> userOptional = getUserByMatchType(identifier, matchType);

    UserDao user = userOptional.orElseThrow(() ->
        new UserNotFoundException("error.user.not-found", Map.of(
            "identifier", identifier,
            "matchType", matchType.name().toLowerCase())));

    return modelMapper.map(user, UserDto.class);
  }

  @Override
  public Optional<UserDao> getUserDaoByUsername(String username) {
    return userRepository.findByUsername(username);
  }

  @Override
  @Transactional
  public UserDto createUser(UserDto userDto) {
    checkUsername(userDto.getUsername());
    checkEmail(userDto.getEmail());

    UserDao newUser = modelMapper.map(userDto, UserDao.class);
    UserDao savedUser = userRepository.save(newUser);

    UserRoleDao userRoleDao = new UserRoleDao();
    userRoleDao.setUser(savedUser);
    userRoleDao.setRole("ROLE_USER");
    userRoleRepository.save(userRoleDao);

    return modelMapper.map(savedUser, UserDto.class);
  }

  @Override
  @Transactional
  public UserDto updateUser(String username, UpdateUserDto updateUserDto) {
    UserDao userDao = userRepository.findByUsername(username)
        .orElseThrow(() -> new UserNotFoundException("error.user.not-found", username));

    if (!username.equals(updateUserDto.getUsername())) {
      SpringUtils.ifNotEmpty(updateUserDto.getUsername(), this::checkUsername);
    }

    if (!userDao.getEmail().equals(updateUserDto.getEmail())) {
      SpringUtils.ifNotEmpty(updateUserDto.getEmail(), this::checkEmail);
    }

    modelMapper.map(updateUserDto, userDao);
    UserDao savedUser = userRepository.save(userDao);
    return modelMapper.map(savedUser, UserDto.class);
  }

  private Optional<UserDao> getUserByMatchType(String identifier, UserMatchTypeEnum matchType) {
    Optional<UserDao> userOptional;
    switch (matchType) {
      case USERNAME:
        userOptional = userRepository.findByUsername(identifier);
        break;
      case EMAIL:
      default:
        userOptional = userRepository.findByEmail(identifier);
        break;
    }
    return userOptional;
  }

  private void checkUsername(String username) {
    userRepository.findByUsername(username).ifPresent(u -> {
      throw new UsernameUnavailableException("error.user.username.unavailable", username);
    });
  }

  private void checkEmail(String email) {
    userRepository.findByEmail(email).ifPresent(u -> {
      throw new EmailUnavailableException("error.user.email.unavailable", email);
    });
  }
}
