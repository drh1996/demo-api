package com.drh.resources.demo.service;

import com.drh.resources.demo.exception.DemoException;
import com.drh.resources.demo.exception.UserNotFoundException;
import com.drh.resources.demo.exception.UserUnauthorizedException;
import com.drh.resources.demo.model.LoginRequest;
import com.drh.resources.demo.model.UserMatchTypeEnum;
import com.drh.resources.demo.persistance.dao.UserDao;
import com.drh.resources.demo.persistance.dto.SessionDto;
import com.drh.resources.demo.persistance.dto.UpdateUserDto;
import com.drh.resources.demo.persistance.dto.UserData;
import com.drh.resources.demo.persistance.dto.UserDto;
import com.drh.resources.demo.service.jwt.model.JwtResponse;
import com.drh.resources.demo.service.jwt.service.JwtService;
import com.drh.resources.demo.service.session.SessionService;
import com.drh.resources.demo.service.user.UserService;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Slf4j
@Service
@RequiredArgsConstructor
public class DemoServiceImpl implements DemoService {

  private final UserService userService;
  private final SessionService sessionService;
  private final JwtService jwtService;
  private final PasswordEncoder passwordEncoder;

  @Override
  public Mono<JwtResponse> loginUser(LoginRequest loginRequest) {
    UserDao userDao = userService.getUserDaoByUsername(loginRequest.getUsername()).orElseThrow(() ->
        new UserUnauthorizedException("error.user.unauthorized", loginRequest.getUsername()));

    if (!passwordEncoder.matches(loginRequest.getPassword(), userDao.getPassword())) {
      log.info("User password does not match");
      throw new UserUnauthorizedException("error.user.unauthorized", loginRequest.getUsername());
    }

    return jwtService.generateJwt(userDao);
  }

  @Override
  public UserDto getUser(String identifier, UserMatchTypeEnum matchType) {
    return userService.getUser(identifier, matchType);
  }

  @Override
  public UserDto createUser(UserDto userDto) {
    return userService.createUser(userDto);
  }

  @Override
  public UserDto updateUser(String name, UpdateUserDto userDto) {
    return userService.updateUser(name, userDto);
  }

  @Override
  public SessionDto getSession(LocalDate date) {
    return sessionService.getSession(date);
  }

  @Override
  public List<SessionDto> getSessions(LocalDate date, int offset, int limit) {
    return sessionService.getSessions(date, offset, limit);
  }

  @Override
  public SessionDto createSession(SessionDto sessionDto) {
    log.debug("Grabbing all users by the usernames provided");

    Map<String, UserDao> userDaoMap = new LinkedHashMap<>(sessionDto.getUsers().size());
    for (UserData userData : sessionDto.getUsers()) {
      String username = userData.getUsername();
      if (userDaoMap.containsKey(username)) {
        throw new DemoException(
            "error.user-data.username.duplicate", username, HttpStatus.BAD_REQUEST);
      }

      userDaoMap.put(username, userService.getUserDaoByUsername(username)
          .orElseThrow(() -> new UserNotFoundException("error.user.not-found", username)));
    }

    log.debug("Creating session with the users found");
    return sessionService.createSession(new ArrayList<>(userDaoMap.values()), sessionDto);
  }

  @Override
  public void deleteSession(UUID uuid) {
    sessionService.deleteSession(uuid);
  }
}
