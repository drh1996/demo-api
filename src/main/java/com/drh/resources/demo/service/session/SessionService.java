package com.drh.resources.demo.service.session;

import com.drh.resources.demo.persistance.dao.UserDao;
import com.drh.resources.demo.persistance.dto.SessionDto;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface SessionService {

  SessionDto getSession(LocalDate date);

  SessionDto createSession(List<UserDao> users, SessionDto sessionDto);

  List<SessionDto> getSessions(LocalDate startDate, int offset, int limit);

  void deleteSession(UUID uuid);
}
