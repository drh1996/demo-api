package com.drh.resources.demo.service.jwt.service;

import com.drh.resources.demo.persistance.dao.UserDao;
import com.drh.resources.demo.service.jwt.model.JwtResponse;
import reactor.core.publisher.Mono;

public interface JwtService {

  Mono<JwtResponse> generateJwt(UserDao user);
}
