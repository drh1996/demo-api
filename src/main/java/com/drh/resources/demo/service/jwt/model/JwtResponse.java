package com.drh.resources.demo.service.jwt.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtResponse {

  private String token;

  @JsonInclude(value = JsonInclude.Include.NON_NULL)
  private String expiration;
}
