package com.drh.resources.demo.service.jwt.service;

import com.drh.resources.demo.configuration.WebClientConfig;
import com.drh.resources.demo.persistance.dao.UserDao;
import com.drh.resources.demo.persistance.dao.UserRoleDao;
import com.drh.resources.demo.service.jwt.model.JwtRequest;
import com.drh.resources.demo.service.jwt.model.JwtResponse;
import com.drh.resources.demo.utils.RestUtils;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ReactiveHttpOutputMessage;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
@RequiredArgsConstructor
public class JwtServiceImpl implements JwtService {

  private final WebClient webClient;
  private final RestUtils restUtils;
  private final WebClientConfig webClientConfig;

  @Value("${demo-security.secret}")
  private String secret;

  @Override
  public Mono<JwtResponse> generateJwt(UserDao user) {
    return webClient.post()
        .uri(builder -> builder.path(webClientConfig.getJwtGenerateUrl()).build())
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .body(createJwtRequest(user))
        .retrieve()
        .bodyToMono(JwtResponse.class)
        .onErrorMap(restUtils::handleRestError);
  }

  private BodyInserter<JwtRequest, ReactiveHttpOutputMessage> createJwtRequest(UserDao user) {
    JwtRequest jwtRequest = new JwtRequest();
    jwtRequest.setUserId(user.getId().toString());
    jwtRequest.setSecret(secret);
    jwtRequest.setClaims(Map.of("roles", createRoles(user.getUserRoles())));
    return BodyInserters.fromValue(jwtRequest);
  }

  private static Set<String> createRoles(Set<UserRoleDao> userRoles) {
    return userRoles.stream()
        .map(UserRoleDao::getRole)
        .collect(Collectors.toSet());
  }
}
