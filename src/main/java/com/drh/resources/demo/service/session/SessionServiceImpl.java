package com.drh.resources.demo.service.session;

import com.drh.resources.demo.exception.SessionInvalidException;
import com.drh.resources.demo.exception.SessionNotFoundException;
import com.drh.resources.demo.persistance.dao.SessionDao;
import com.drh.resources.demo.persistance.dao.UserDao;
import com.drh.resources.demo.persistance.dao.UserSessionEntryDao;
import com.drh.resources.demo.persistance.dao.UserSessionEntryKey;
import com.drh.resources.demo.persistance.dto.SessionDto;
import com.drh.resources.demo.persistance.dto.UserData;
import com.drh.resources.demo.persistance.repository.SessionRepository;
import com.drh.resources.demo.persistance.repository.UserSessionEntryRepository;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class SessionServiceImpl implements SessionService {

  private final SessionRepository sessionRepository;
  private final UserSessionEntryRepository userSessionEntryRepository;
  private final ModelMapper modelMapper;

  @Override
  public SessionDto getSession(LocalDate date) {
    SessionDao sessionDao = sessionRepository.findByDate(date)
        .orElseThrow(() -> new SessionNotFoundException("error.session.not-found.date", date));

    return modelMapper.map(sessionDao, SessionDto.class);
  }

  @Override
  @Transactional
  public SessionDto createSession(List<UserDao> users, SessionDto sessionDto) {
    validateUserData(sessionDto.getUsers());

    SessionDao savedSession = sessionRepository.save(
        modelMapper.map(sessionDto, SessionDao.class));

    Set<UserSessionEntryDao> entryDaos = new HashSet<>(users.size());
    for (int i = 0; i < users.size(); i++) {
      UserDao userDao = users.get(i);

      UserSessionEntryKey key = new UserSessionEntryKey();
      key.setSessionId(savedSession.getId());
      key.setUserId(userDao.getId());

      UserSessionEntryDao entryDao = new UserSessionEntryDao();
      entryDao.setId(key);
      entryDao.setSession(savedSession);
      entryDao.setUser(userDao);

      UserData userData = sessionDto.getUsers().get(i);
      entryDao.setInForPence(userData.getInForPence());
      entryDao.setOutForPence(userData.getOutForPence());
      entryDao.setFoodCostPence(userData.getFoodCostPence());
      entryDao.setPaidForFood(userData.getPaidForFood());

      entryDaos.add(entryDao);
    }

    userSessionEntryRepository.saveAll(entryDaos);
    savedSession.setUserSessionEntries(entryDaos);

    return modelMapper.map(savedSession, SessionDto.class);
  }

  @Override
  public List<SessionDto> getSessions(LocalDate startDate, int offset, int limit) {
    Page<SessionDao> page = sessionRepository.findAndOrderByDateDesc(
        startDate,
        PageRequest.of(offset, limit, Sort.by("date").descending()));

    if (page.isEmpty()) {
      throw new SessionNotFoundException("error.sessions.not-found", Map.of(
          "startDate", modelMapper.map(startDate, String.class),
          "offset", offset,
          "limit", limit
      ));
    }

    return page.get()
        .map(sessionDao -> modelMapper.map(sessionDao, SessionDto.class))
        .collect(Collectors.toList());
  }

  @Override
  public void deleteSession(UUID uuid) {
    SessionDao sessionDao = sessionRepository.findById(uuid)
        .orElseThrow(() -> new SessionNotFoundException("error.session.not-found.id", uuid));

    sessionRepository.delete(sessionDao);
  }

  private void validateUserData(List<UserData> users) {
    int totalInFor = 0;
    int totalOutFor = 0;
    for (UserData userData : users) {
      totalInFor += userData.getInForPence();
      totalOutFor += userData.getOutForPence();
    }

    if (totalInFor == totalOutFor) {
      return;
    }

    if (totalInFor > totalOutFor) {
      throw new SessionInvalidException(
          "error.user-data.out-for.missing", totalInFor - totalOutFor);
    }

    throw new SessionInvalidException("error.user-data.out-for.extra", totalOutFor - totalInFor);
  }
}
