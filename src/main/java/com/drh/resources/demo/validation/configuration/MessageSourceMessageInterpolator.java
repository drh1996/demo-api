package com.drh.resources.demo.validation.configuration;

import java.util.Locale;
import javax.validation.MessageInterpolator;
import javax.validation.Validation;
import lombok.RequiredArgsConstructor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@RequiredArgsConstructor
public class MessageSourceMessageInterpolator implements MessageInterpolator {

  private final MessageInterpolator defaultInterpolator = Validation.byDefaultProvider()
      .configure()
      .getDefaultMessageInterpolator();

  private final ReloadableResourceBundleMessageSource messageSource;

  @Override
  public String interpolate(String messageTemplate, Context context) {
    return interpolate(messageTemplate, context, Locale.UK);
  }

  @Override
  public String interpolate(String messageTemplate, Context context, Locale locale) {
    String msg = messageSource.getMessage(messageTemplate, null, messageTemplate, locale);
    return defaultInterpolator.interpolate(msg, context, locale);
  }
}
