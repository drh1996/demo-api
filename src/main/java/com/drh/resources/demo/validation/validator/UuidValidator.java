package com.drh.resources.demo.validation.validator;

import com.drh.resources.demo.validation.validator.annotation.Uuid;
import java.util.UUID;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UuidValidator implements ConstraintValidator<Uuid, String> {

  @Override
  public boolean isValid(String uuid, ConstraintValidatorContext context) {
    try {
      UUID.fromString(uuid);
    } catch (Exception e) {
      return false;
    }
    return true;
  }
}
