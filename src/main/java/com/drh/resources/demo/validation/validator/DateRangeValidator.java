package com.drh.resources.demo.validation.validator;

import com.drh.resources.demo.validation.validator.annotation.DateRange;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;

@Slf4j
public class DateRangeValidator implements ConstraintValidator<DateRange, String> {

  private static final LocalDate MIN = LocalDate.of(1, 1, 1);
  private static final LocalDate MAX = LocalDate.of(9999, 12, 31);

  private DateTimeFormatter format;
  private LocalDate from;
  private String fromRaw;
  private LocalDate to;
  private String toRaw;

  @Override
  public void initialize(DateRange constraintAnnotation) {
    format = DateTimeFormatter.ofPattern(constraintAnnotation.format());

    fromRaw = constraintAnnotation.from();
    if (fromRaw.equals(DateRange.ANY)) {
      from = MIN;
      fromRaw = format.format(from);
    } else if (fromRaw.equals(DateRange.EPOCH)) {
      from = LocalDate.EPOCH;
      fromRaw = format.format(from);
    } else if (!fromRaw.equals(DateRange.NOW)) {
      from = LocalDate.parse(fromRaw, format);
    }

    toRaw = constraintAnnotation.to();
    if (toRaw.equals(DateRange.ANY)) {
      to = MAX;
      toRaw = format.format(to);
    } else if (toRaw.equals(DateRange.EPOCH)) {
      to = LocalDate.EPOCH;
      toRaw = format.format(to);
    } else if (!toRaw.equals(DateRange.NOW)) {
      to = LocalDate.parse(toRaw, format);
    }

    validateRange();
  }

  @Override
  public boolean isValid(String date, ConstraintValidatorContext context) {
    LocalDate to = getTo();
    LocalDate from = getFrom();
    saveDateRangeParsed(context, from, to);

    if (date == null) {
      return true;
    }

    LocalDate current;
    try {
      current = LocalDate.parse(date, format);
    } catch (Exception e) {
      log.debug("Failed to parse date '{}' with format '{}' with message: {}",
          date, format.toString(), e.getMessage());
      return false;
    }

    return (current.isAfter(from) && current.isBefore(to))
        || current.equals(from)
        || current.equals(to);
  }

  private void validateRange() {
    LocalDate to = getTo();
    LocalDate from = getFrom();
    if (from.isAfter(to)) {
      throw new IllegalArgumentException(
          "to '" + format.format(to) + "' is before from '" + format.format(from) + '\'');
    }
  }

  private void saveDateRangeParsed(ConstraintValidatorContext c, LocalDate from, LocalDate to) {
    c.unwrap(HibernateConstraintValidatorContext.class)
        .addExpressionVariable("toParsed", getFormatted(to, toRaw))
        .addExpressionVariable("fromParsed", getFormatted(from, fromRaw));
  }

  private String getFormatted(LocalDate date, String raw) {
    String formatted = raw;
    if (raw.equals(DateRange.NOW)) {
      formatted = format.format(date);
    }
    return formatted;
  }

  private LocalDate getFrom() {
    return from == null ? LocalDate.now() : from;
  }

  private LocalDate getTo() {
    return to == null ? LocalDate.now() : to;
  }
}
