package com.drh.resources.demo.validation.validator.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;

import com.drh.resources.demo.utils.consts.DateFormats;
import com.drh.resources.demo.validation.validator.DateRangeValidator;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Constraint(validatedBy = DateRangeValidator.class)
@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DateRange {

  //any date
  String ANY = "any";
  //current date
  String NOW = "now";
  //epoch date
  String EPOCH = "epoch";

  String message() default "error.validation.date-time.range.default";

  String format() default DateFormats.DATE;

  String from() default ANY;

  String to() default ANY;

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
