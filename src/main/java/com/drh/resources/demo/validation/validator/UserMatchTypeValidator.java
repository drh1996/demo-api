package com.drh.resources.demo.validation.validator;

import com.drh.resources.demo.model.UserMatchTypeEnum;
import com.drh.resources.demo.validation.validator.annotation.UserMatchType;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UserMatchTypeValidator implements ConstraintValidator<UserMatchType, String> {

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    return UserMatchTypeEnum.parseMatchType(value) != null;
  }
}
