package com.drh.resources.demo.validation.validator;

import com.drh.resources.demo.validation.validator.annotation.DateTimeFormat;
import java.time.format.DateTimeFormatter;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

@Slf4j
public class DateTimeValidator implements ConstraintValidator<DateTimeFormat, String> {

  private DateTimeFormatter format;
  private boolean allowEmpty;

  @Override
  public void initialize(DateTimeFormat constraintAnnotation) {
    format = DateTimeFormatter.ofPattern(constraintAnnotation.format());
    allowEmpty = constraintAnnotation.allowEmpty();
  }

  @Override
  public boolean isValid(String date, ConstraintValidatorContext context) {
    if (ObjectUtils.isEmpty(date)) {
      return allowEmpty;
    }

    try {
      format.parse(date);
      return true;
    } catch (Exception e) {
      log.debug("Failed to parse date '{}' with format '{}'", date, format, e);
    }

    return false;
  }
}
