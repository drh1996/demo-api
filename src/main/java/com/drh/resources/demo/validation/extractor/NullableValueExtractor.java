package com.drh.resources.demo.validation.extractor;

import com.drh.resources.demo.persistance.dto.Nullable;
import javax.validation.valueextraction.ExtractedValue;
import javax.validation.valueextraction.ValueExtractor;

public class NullableValueExtractor implements ValueExtractor<Nullable<@ExtractedValue ?>> {

  @Override
  public void extractValues(Nullable<?> originalValue, ValueReceiver receiver) {
    //gets value from Nullable so it can be validated
    receiver.value(null, originalValue.get());
  }
}
