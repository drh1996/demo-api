package com.drh.resources.demo.validation.validator;

import com.drh.resources.demo.validation.validator.annotation.Password;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import lombok.RequiredArgsConstructor;
import org.passay.PasswordData;
import org.passay.RuleResult;

@RequiredArgsConstructor
public class PasswordValidator implements ConstraintValidator<Password, String> {

  private final org.passay.PasswordValidator validator;

  @Override
  public boolean isValid(String password, ConstraintValidatorContext context) {
    if (password == null) {
      return true;
    }

    RuleResult result = validator.validate(new PasswordData(password));
    if (result.isValid()) {
      return true;
    }

    context.disableDefaultConstraintViolation();
    for (String error : validator.getMessages(result)) {
      context.buildConstraintViolationWithTemplate(error).addConstraintViolation();
    }
    return false;
  }
}
