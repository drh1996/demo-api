package com.drh.resources.demo.validation.factory;

import com.drh.resources.demo.exception.ParameterValidationException;
import com.drh.resources.demo.validation.configuration.MessageSourceMessageInterpolator;
import com.drh.resources.demo.validation.extractor.NullableValueExtractor;
import javax.validation.Configuration;
import lombok.NonNull;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

public class CustomLocalValidatorFactoryBean extends LocalValidatorFactoryBean {

  @Override
  protected void postProcessConfiguration(@NonNull Configuration<?> configuration) {
    super.postProcessConfiguration(configuration);

    //add custom Nullable value extractor
    configuration.addValueExtractor(new NullableValueExtractor());
  }

  @Override
  public void validate(@NonNull Object target, @NonNull Errors errors) {
    super.validate(target, errors);
  }

  public void validateParam(
      @NonNull Class<?> targetClass,
      @NonNull String fieldName,
      Object fieldValue,
      String paramName) {

    String newParamName = paramName == null ? fieldName : paramName;

    BeanPropertyBindingResult result = new BeanPropertyBindingResult(fieldValue, newParamName);
    validateValue(targetClass, fieldName, fieldValue, result);

    if (result.hasFieldErrors()) {
      throw new ParameterValidationException(fieldValue, newParamName, result.getFieldErrors());
    }
  }
}
