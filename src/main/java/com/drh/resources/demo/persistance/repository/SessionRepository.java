package com.drh.resources.demo.persistance.repository;

import com.drh.resources.demo.persistance.dao.SessionDao;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionRepository extends JpaRepository<SessionDao, UUID> {

  Optional<SessionDao> findByDate(LocalDate date);

  @Query("SELECT s FROM SessionDao s WHERE s.date <= :date")
  Page<SessionDao> findAndOrderByDateDesc(@Param("date") LocalDate date, Pageable pageable);
}
