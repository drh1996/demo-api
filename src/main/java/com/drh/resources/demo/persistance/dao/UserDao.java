package com.drh.resources.demo.persistance.dao;

import com.drh.resources.demo.persistance.converter.PasswordConverter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

@Getter
@Setter
@Entity
@Table(name = "user")
public class UserDao {

  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  @Column(columnDefinition = "BINARY(16)")
  private UUID id;

  @Column(name = "email", unique = true, nullable = false)
  private String email;

  @Column(name = "username", unique = true, nullable = false)
  private String username;

  @Convert(converter = PasswordConverter.class)
  @Column(name = "password", nullable = false)
  private String password;

  @Column(name = "first_name", nullable = false)
  private String firstName;

  @Column(name = "last_name", nullable = false)
  private String lastName;

  @Column(name = "nickname")
  private String nickname;

  @Column(name = "date_of_birth", nullable = false)
  private LocalDate dateOfBirth;

  @Column(name = "created_ts", nullable = false)
  private LocalDateTime createdTs;

  @Column(name = "updated_ts")
  private LocalDateTime updatedTs;

  @OneToMany(mappedBy = "user")
  private Set<UserSessionEntryDao> userSessionEntries;

  @OneToMany(mappedBy = "user")
  private Set<UserRoleDao> userRoles;

  @PrePersist
  private void prePersist() {
    createdTs = LocalDateTime.now();
  }

  @PreUpdate
  private void preUpdate() {
    updatedTs = LocalDateTime.now();
  }
}
