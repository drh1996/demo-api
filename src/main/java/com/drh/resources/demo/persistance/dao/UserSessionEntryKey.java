package com.drh.resources.demo.persistance.dao;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Embeddable
public class UserSessionEntryKey implements Serializable {

  @Column(name = "session_id", columnDefinition = "BINARY(16)")
  private UUID sessionId;

  @Column(name = "user_id", columnDefinition = "BINARY(16)")
  private UUID userId;

  @Override
  public boolean equals(Object obj) {
    if (obj == null || !obj.getClass().equals(UserSessionEntryKey.class)) {
      return false;
    }

    UserSessionEntryKey other = (UserSessionEntryKey) obj;
    return sessionId.equals(other.sessionId) && userId.equals(other.userId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sessionId.hashCode() + userId.hashCode());
  }
}
