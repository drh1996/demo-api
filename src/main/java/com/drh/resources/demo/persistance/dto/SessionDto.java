package com.drh.resources.demo.persistance.dto;

import com.drh.resources.demo.utils.consts.RegexPatterns;
import com.drh.resources.demo.validation.validator.annotation.DateRange;
import com.drh.resources.demo.validation.validator.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SessionDto {

  @JsonProperty(access = Access.READ_ONLY)
  private UUID id;

  @DateRange(to = DateRange.NOW)
  @DateTimeFormat(format = "dd/MM/yyyy", allowEmpty = false)
  private String date;

  @Pattern(regexp = RegexPatterns.TEXT, message = "error.validation.regex.default")
  private String comment;

  @Valid
  @NotNull(message = "error.validation.null.list")
  @NotEmpty(message = "error.validation.empty.list")
  private List<UserData> users;
}
