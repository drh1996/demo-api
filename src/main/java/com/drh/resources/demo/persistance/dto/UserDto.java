package com.drh.resources.demo.persistance.dto;

import com.drh.resources.demo.utils.consts.DateFormats;
import com.drh.resources.demo.utils.consts.RegexPatterns;
import com.drh.resources.demo.validation.validator.annotation.DateRange;
import com.drh.resources.demo.validation.validator.annotation.DateTimeFormat;
import com.drh.resources.demo.validation.validator.annotation.Email;
import com.drh.resources.demo.validation.validator.annotation.Password;
import com.drh.resources.demo.validation.validator.annotation.Username;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto {

  @JsonIgnore
  private UUID userId;

  @Email
  @NotNull(message = "error.validation.null.default")
  private String email;

  @Username
  @NotNull(message = "error.validation.null.default")
  private String username;

  @Password
  @NotNull(message = "error.validation.null.default")
  @JsonProperty(access = Access.WRITE_ONLY)
  private String password;

  @NotNull(message = "error.validation.null.default")
  @Size(min = 1, max = 64, message = "error.validation.size.string.default")
  @Pattern(regexp = RegexPatterns.NAME, message = "error.validation.regex.default")
  private String firstName;

  @NotNull(message = "error.validation.null.default")
  @Size(min = 1, max = 64, message = "error.validation.size.string.default")
  @Pattern(regexp = RegexPatterns.NAME, message = "error.validation.regex.default")
  private String lastName;

  @Size(min = 2, max = 20, message = "error.validation.size.string.default")
  @Pattern(regexp = RegexPatterns.NAME, message = "error.validation.regex.default")
  private String nickname;

  @DateRange(from = "01/01/1900", to = DateRange.NOW)
  @DateTimeFormat(format = DateFormats.DATE, allowEmpty = false)
  private String dob;

  @JsonProperty(access = Access.READ_ONLY)
  private String createdTs;

  @JsonProperty(access = Access.READ_ONLY)
  private String updatedTs;

  @JsonProperty(access = Access.READ_ONLY)
  private List<String> sessions;

}
