package com.drh.resources.demo.persistance.dto;

import com.drh.resources.demo.utils.consts.DateFormats;
import com.drh.resources.demo.utils.consts.RegexPatterns;
import com.drh.resources.demo.validation.validator.annotation.DateRange;
import com.drh.resources.demo.validation.validator.annotation.DateTimeFormat;
import com.drh.resources.demo.validation.validator.annotation.Email;
import com.drh.resources.demo.validation.validator.annotation.Password;
import com.drh.resources.demo.validation.validator.annotation.Username;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateUserDto {

  @Email
  private String email;

  @Username
  private String username;

  @Password
  private String password;

  @Size(min = 1, max = 64, message = "error.validation.size.string.default")
  @Pattern(regexp = RegexPatterns.NAME, message = "error.validation.regex.default")
  private String firstName;

  @Size(min = 1, max = 64, message = "error.validation.size.string.default")
  @Pattern(regexp = RegexPatterns.NAME, message = "error.validation.regex.default")
  private String lastName;

  private Nullable<
      @Size(min = 3, max = 20, message = "error.validation.size.string.default")
      @Pattern(regexp = RegexPatterns.NAME, message = "error.validation.regex.default")
          String> nickname;

  @DateRange(from = "01/01/1900", to = DateRange.NOW)
  @DateTimeFormat(format = DateFormats.DATE, allowEmpty = false)
  private String dob;

  //for deserializing nickname
  public void setNickname(String nickname) {
    this.nickname = Nullable.of(nickname);
  }

}
