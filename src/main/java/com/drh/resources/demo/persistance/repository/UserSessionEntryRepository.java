package com.drh.resources.demo.persistance.repository;

import com.drh.resources.demo.persistance.dao.UserSessionEntryDao;
import com.drh.resources.demo.persistance.dao.UserSessionEntryKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSessionEntryRepository extends
    JpaRepository<UserSessionEntryDao, UserSessionEntryKey> {

}
