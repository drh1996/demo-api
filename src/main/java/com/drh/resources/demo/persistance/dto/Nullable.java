package com.drh.resources.demo.persistance.dto;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class Nullable<T> {

  private final T obj;

  public static <T> Nullable<T> of(T obj) {
    return new Nullable<>(obj);
  }

  public T get() {
    return obj;
  }
}
