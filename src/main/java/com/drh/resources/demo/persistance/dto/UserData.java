package com.drh.resources.demo.persistance.dto;

import com.drh.resources.demo.validation.validator.annotation.Username;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserData {

  @Username
  @NotNull(message = "error.validation.null.default")
  private String username;

  @NotNull(message = "error.validation.null.default")
  @Min(value = 0L, message = "error.validation.min.int.negative")
  private Integer inForPence;

  @NotNull(message = "error.validation.null.default")
  @Min(value = 0L, message = "error.validation.min.int.negative")
  private Integer outForPence;

  @NotNull(message = "error.validation.null.default")
  @Min(value = 0L, message = "error.validation.min.int.negative")
  private Integer foodCostPence;

  @NotNull(message = "error.validation.null.default")
  private Boolean paidForFood;
}
