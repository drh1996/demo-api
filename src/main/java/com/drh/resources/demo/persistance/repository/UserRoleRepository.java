package com.drh.resources.demo.persistance.repository;

import com.drh.resources.demo.persistance.dao.UserRoleDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRoleDao, Long> {

}
