package com.drh.resources.demo.persistance.repository;

import com.drh.resources.demo.persistance.dao.UserDao;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserDao, UUID> {

  Optional<UserDao> findByUsername(String username);

  Optional<UserDao> findByEmail(String email);

}
