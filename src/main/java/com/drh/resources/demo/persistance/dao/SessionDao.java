package com.drh.resources.demo.persistance.dao;

import java.time.LocalDate;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

@Getter
@Setter
@Entity
@Table(name = "session")
public class SessionDao {

  @Id
  @GeneratedValue(generator = "uuid2")
  @GenericGenerator(name = "uuid2", strategy = "uuid2")
  @Column(columnDefinition = "BINARY(16)")
  private UUID id;

  private LocalDate date;

  @Column(columnDefinition = "TEXT")
  private String comment;

  @OneToMany(mappedBy = "session")
  private Set<UserSessionEntryDao> userSessionEntries;
}
