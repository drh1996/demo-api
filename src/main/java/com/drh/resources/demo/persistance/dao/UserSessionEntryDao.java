package com.drh.resources.demo.persistance.dao;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "user_session_entry")
public class UserSessionEntryDao {

  @EmbeddedId
  private UserSessionEntryKey id;

  @ManyToOne
  @MapsId("sessionId")
  @JoinColumn(name = "session_id")
  private SessionDao session;

  @ManyToOne
  @MapsId("userId")
  @JoinColumn(name = "user_id")
  private UserDao user;

  @Column(name = "in_for_pence", nullable = false)
  private int inForPence;

  @Column(name = "out_for_pence", nullable = false)
  private int outForPence;

  @Column(name = "food_cost_pence", nullable = false)
  private int foodCostPence;

  @Column(name = "paid_for_food", nullable = false)
  private boolean paidForFood;
}
