DROP TABLE IF EXISTS `user_session_entry`;
DROP TABLE IF EXISTS `session`;
DROP TABLE IF EXISTS `user_role`;
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  id UUID PRIMARY KEY,
  email VARCHAR(254) NOT NULL UNIQUE,
  username VARCHAR(64) NOT NULL UNIQUE,
  password VARCHAR(64) NOT NULL,
  first_name VARCHAR(64) NOT NULL,
  last_name VARCHAR(64) NOT NULL,
  nickname VARCHAR(20),
  date_of_birth DATE NOT NULL,
  created_ts TIMESTAMP NOT NULL,
  updated_ts TIMESTAMP
);

CREATE TABLE `session` (
  id UUID PRIMARY KEY,
  `date` DATE NOT NULL,
  comment TEXT
);

CREATE TABLE `user_session_entry` (
  session_id UUID NOT NULL,
  user_id UUID NOT NULL,
  in_for_pence INT NOT NULL,
  out_for_pence INT NOT NULL,
  food_cost_pence INT NOT NULL,
  paid_for_food TINYINT(1) NOT NULL,

  PRIMARY KEY (session_id, user_id),
  FOREIGN KEY (session_id) REFERENCES `session`(id) ON DELETE CASCADE,
  FOREIGN KEY (user_id) REFERENCES `user`(id) ON DELETE CASCADE
);

CREATE TABLE `user_role` (
  id INT PRIMARY KEY AUTO_INCREMENT,
  user_id UUID NOT NULL,
  role VARCHAR(20) NOT NULL,

  FOREIGN KEY (user_id) REFERENCES `user`(id) ON DELETE CASCADE
);

INSERT INTO `user` (id, email, username, password, first_name, last_name, nickname, date_of_birth, created_ts, updated_ts) VALUES
('145a3631-70eb-47f6-b70d-e6f83557f8da', 'test@email.com', 'testUser', '$2a$10$a5FFP/ykk4fbr/mY/9xBwOrjIktx8A06JcKdJ3sfrGhrSBQQPPRGW', 'John', 'Doe', 'JD', '1970-01-01', '2021-01-01 12:00:00', '2021-01-01 12:00:01'),
('e85b3a36-3c6b-4c81-8e59-847e56099d72', 'test2@email.com', 'testUser2', '$2a$10$a5FFP/ykk4fbr/mY/9xBwOrjIktx8A06JcKdJ3sfrGhrSBQQPPRGW', 'Jane', 'Doe', 'Jan', '1970-01-01', '2021-01-01 12:00:00', '2021-01-01 12:00:01'),
('f2b75218-20bc-477a-b507-36bcc7427c1b', 'test3@email.com', 'testUser3', '$2a$10$a5FFP/ykk4fbr/mY/9xBwOrjIktx8A06JcKdJ3sfrGhrSBQQPPRGW', 'Ben', 'Smith', 'Benny', '1970-01-01', '2021-01-01 12:00:00', '2021-01-01 12:00:01');

INSERT INTO `session` (id, `date`, comment) VALUES
('4152e28b-3006-4e29-86e8-e4d991953c13', '2021-01-01', 'Example Comment');

INSERT INTO `user_session_entry` (session_id, user_id, in_for_pence, out_for_pence, food_cost_pence, paid_for_food) VALUES
('4152e28b-3006-4e29-86e8-e4d991953c13', '145a3631-70eb-47f6-b70d-e6f83557f8da', 1000, 2500, 0, 1),
('4152e28b-3006-4e29-86e8-e4d991953c13', 'e85b3a36-3c6b-4c81-8e59-847e56099d72', 1000, 500, 1007, 0),
('4152e28b-3006-4e29-86e8-e4d991953c13', 'f2b75218-20bc-477a-b507-36bcc7427c1b', 1000, 0, 1050, 0);

INSERT INTO `user_role` (id, user_id, role) VALUES
(1, '145a3631-70eb-47f6-b70d-e6f83557f8da', 'ROLE_OWNER'),
(2, '145a3631-70eb-47f6-b70d-e6f83557f8da', 'ROLE_USER'),
(3, 'e85b3a36-3c6b-4c81-8e59-847e56099d72', 'ROLE_USER'),
(4, 'f2b75218-20bc-477a-b507-36bcc7427c1b', 'ROLE_USER');