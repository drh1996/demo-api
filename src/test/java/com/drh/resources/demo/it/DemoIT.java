package com.drh.resources.demo.it;

import static com.drh.resources.demo.support.MessageSourceInterpolator.dateRangeMessage;
import static com.drh.resources.demo.support.MessageSourceInterpolator.dateTimeFormatMessage;
import static com.drh.resources.demo.support.MessageSourceInterpolator.message;
import static com.drh.resources.demo.support.MessageSourceInterpolator.sizeMessage;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.anyRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.anyUrl;
import static com.github.tomakehurst.wiremock.client.WireMock.badRequest;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;
import static com.github.tomakehurst.wiremock.client.WireMock.postRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.matchesRegex;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.drh.resources.demo.support.DataLoader;
import com.drh.resources.demo.support.JsonUtils;
import com.drh.resources.demo.support.MessageSourceInterpolator;
import com.drh.resources.demo.utils.consts.Endpoints;
import com.drh.resources.demo.validation.validator.annotation.DateRange;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.transaction.annotation.Transactional;

@ExtendWith(SpringExtension.class)
@Transactional
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureMockMvc
@AutoConfigureWireMock(port = 0)
@DisplayName("Integration Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class DemoIT {

  @Value("${demo-security.secret}")
  private String jwtSecret;

  @Autowired
  private MockMvc mockMvc;

  @Nested
  @DisplayName("Login Integration Tests")
  public class LoginIT {
    @AfterEach
    public void afterEach() {
      WireMock.reset();
    }

    @Test
    public void login_successfully() throws Exception {
      stubFor(WireMock.post(urlEqualTo("/jwt/generate"))
          .willReturn(okJson(DataLoader.loadFile("data/jwt/jwtGeneratorResponse.json"))));

      MvcResult mvcResult = mockMvc.perform(post(Endpoints.LOGIN_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/loginRequest.json")))
          .andExpect(request().asyncStarted())
          .andReturn();

      mockMvc.perform(asyncDispatch(mvcResult))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.token").value(matchesRegex("Bearer \\w+\\.\\w+\\.[\\w-]+")));

      verify(WireMock.exactly(1), postRequestedFor(urlEqualTo("/jwt/generate")));
    }

    @Test
    public void login_jwt_generator_non_200_response() throws Exception {
      stubFor(WireMock.post(urlEqualTo("/jwt/generate"))
          .willReturn(badRequest().withBody("{\"error\": \"dummy\"}")));

      MvcResult mvcResult = mockMvc.perform(post(Endpoints.LOGIN_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/loginRequest.json")))
          .andExpect(request().asyncStarted())
          .andReturn();

      mockMvc.perform(asyncDispatch(mvcResult))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.BAD_REQUEST.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.BAD_REQUEST.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.jwt.response-error", HttpStatus.BAD_REQUEST.getReasonPhrase())))
          .andExpect(jsonPath("$.moreInformation").value("{\"error\": \"dummy\"}"));

      verify(WireMock.exactly(1), postRequestedFor(urlEqualTo("/jwt/generate")));
    }

    @Test
    public void login_jwt_generator_read_timeout() throws Exception {
      stubFor(WireMock.post(urlEqualTo("/jwt/generate"))
          .willReturn(okJson(DataLoader.loadFile("data/jwt/jwtGeneratorResponse.json"))
              .withFixedDelay(2000)));

      MvcResult mvcResult = mockMvc.perform(post(Endpoints.LOGIN_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/loginRequest.json")))
          .andExpect(request().asyncStarted())
          .andReturn();

      mockMvc.perform(asyncDispatch(mvcResult))
          .andExpect(status().isInternalServerError())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.INTERNAL_SERVER_ERROR.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.jwt.read-timeout")));

      verify(WireMock.exactly(1), postRequestedFor(urlEqualTo("/jwt/generate")));
    }

    @Test
    public void login_jwt_generator_connection_reset() throws Exception {
      stubFor(WireMock.post(urlEqualTo("/jwt/generate"))
          .willReturn(aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER)));

      MvcResult mvcResult = mockMvc.perform(post(Endpoints.LOGIN_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/loginRequest.json")))
          .andExpect(request().asyncStarted())
          .andReturn();

      mockMvc.perform(asyncDispatch(mvcResult))
          .andExpect(status().isInternalServerError())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.INTERNAL_SERVER_ERROR.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.jwt.request")));

      verify(WireMock.exactly(1), postRequestedFor(urlEqualTo("/jwt/generate")));
    }

    @Test
    public void login_invalid_fields() throws Exception {
      JsonObject request = DataLoader.loadFileAsElement("data/user/loginRequest.json").getAsJsonObject();
      request.addProperty("username", "a");
      request.add("password", JsonNull.INSTANCE);

      mockMvc.perform(post(Endpoints.LOGIN_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request)))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.message").value(message("error.validation.body.default")))
          .andExpect(jsonPath("$.errors[?(@.name == 'username' && @.provided == 'a')]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'username' && @.provided == 'a')].messages[*]",
              arrayContaining(1, sizeMessage(3, 64))))
          .andExpect(jsonPath("$.errors[?(@.name == 'password' && @.provided == null)]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'password' && @.provided == null)].messages[*]",
              arrayContaining(1, "error.validation.null.default")));

      verify(WireMock.exactly(0), anyRequestedFor(anyUrl()));
    }

    @Test
    public void login_username_not_found() throws Exception {
      JsonObject request = DataLoader.loadFileAsElement("data/user/loginRequest.json").getAsJsonObject();
      request.addProperty("username", "notfound");

      mockMvc.perform(post(Endpoints.LOGIN_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request)))
          .andExpect(status().isUnauthorized())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.UNAUTHORIZED.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.UNAUTHORIZED.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.user.unauthorized")))
          .andExpect(jsonPath("$.moreInformation").value("notfound"));
    }

    @Test
    public void login_password_incorrect() throws Exception {
      JsonObject request = DataLoader.loadFileAsElement("data/user/loginRequest.json").getAsJsonObject();
      request.addProperty("password", "incorrect");

      mockMvc.perform(post(Endpoints.LOGIN_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request)))
          .andExpect(status().isUnauthorized())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.UNAUTHORIZED.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.UNAUTHORIZED.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.user.unauthorized")))
          .andExpect(jsonPath("$.moreInformation").value("testUser"));
    }
  }

  @Nested
  @DisplayName("Get User Integration Tests")
  public class GetUserIT {
    @Test
    public void get_user_by_email_successfully() throws Exception {
      mockMvc.perform(get(Endpoints.GET_USER, "test@email.com")
              .queryParam("matchType", "email")
              .with(jwt()))
          .andExpect(status().isOk())
          .andExpect(content().json(DataLoader.loadFile("data/user/userDtoResponse.json")));
    }

    @Test
    public void get_user_by_username_successfully() throws Exception {
      mockMvc.perform(get(Endpoints.GET_USER, "testUser")
              .queryParam("matchType", "username")
              .with(jwt()))
          .andExpect(status().isOk())
          .andExpect(content().json(DataLoader.loadFile("data/user/userDtoResponse.json")));
    }

    @Test
    public void get_user_no_jwt() throws Exception {
      mockMvc.perform(get(Endpoints.GET_USER, "test@email.com")
              .queryParam("matchType", "email"))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void get_user_invalid_jwt() throws Exception {
      mockMvc.perform(get(Endpoints.GET_USER, "test@email.com")
              .queryParam("matchType", "email")
              .with(invalidJwt()))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void get_user_by_email_not_found() throws Exception {
      mockMvc.perform(get(Endpoints.GET_USER, "notfound@email.com")
              .queryParam("matchType", "email")
              .with(jwt()))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.NOT_FOUND.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.NOT_FOUND.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.user.not-found")))
          .andExpect(jsonPath("$.moreInformation", hasEntries(Map.of(
              "identifier", "notfound@email.com",
              "matchType", "email"))));
    }

    @Test
    public void get_user_by_username_not_found() throws Exception {
      mockMvc.perform(get(Endpoints.GET_USER, "notfound")
              .queryParam("matchType", "username")
              .with(jwt()))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.NOT_FOUND.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.NOT_FOUND.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.user.not-found")))
          .andExpect(jsonPath("$.moreInformation", hasEntries(Map.of(
              "identifier", "notfound",
              "matchType", "username"))));
    }

    @Test
    public void get_user_match_type() throws Exception {
      mockMvc.perform(get(Endpoints.GET_USER, "test@email.com")
              .queryParam("matchType", "invalid")
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.message").value(message("error.validation.param.default")))
          .andExpect(jsonPath("$.errors[?(@.name == 'matchType' && @.provided == 'invalid')]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'matchType' && @.provided == 'invalid')].messages[*]",
              arrayContaining(1, "error.validation.user-match-type.default")));
    }

    @Test
    public void get_user_identifier() throws Exception {
      mockMvc.perform(get(Endpoints.GET_USER, "a")
              .queryParam("matchType", "email")
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.message").value(message("error.validation.param.default")))
          .andExpect(jsonPath("$.errors[?(@.name == 'identifier' && @.provided == 'a')]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'identifier' && @.provided == 'a')].messages[*]",
              arrayContaining(2,sizeMessage(3, 254), "error.validation.email.default")));
    }

    @Test
    public void get_user_missing_match_type_param() throws Exception {
      mockMvc.perform(get(Endpoints.GET_USER, "a")
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.BAD_REQUEST.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.BAD_REQUEST.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.validation.query-param.missing")))
          .andExpect(jsonPath("$.moreInformation").value("matchType"));
    }
  }

  @Nested
  @DisplayName("Create User Integration Tests")
  public class CreateUserIT {
    @Test
    public void create_user_successfully() throws Exception {
      mockMvc.perform(post(Endpoints.CREATE_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/createUserRequest.json"))
              .with(jwt()))
          .andExpect(status().isCreated())
          .andExpect(jsonPath("$.email").value("test4@email.com"))
          .andExpect(jsonPath("$.username").value("testUser4"))
          .andExpect(jsonPath("$.firstName").value("New"))
          .andExpect(jsonPath("$.lastName").value("Human"))
          .andExpect(jsonPath("$.nickname").value("Definitely Human"))
          .andExpect(jsonPath("$.dob").value("01/01/1900"))
          .andExpect(jsonPath("$.createdTs").value(matchesRegex("\\d{2}/\\d{2}/\\d{4} \\d{2}:\\d{2}:\\d{2}")))
          .andExpect(jsonPath("$.updatedTs").value(nullValue()));
    }

    @Test
    public void create_user_no_jwt() throws Exception {
      mockMvc.perform(post(Endpoints.CREATE_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/createUserRequest.json")))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void create_user_invalid_jwt() throws Exception {
      mockMvc.perform(post(Endpoints.CREATE_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/createUserRequest.json"))
              .with(invalidJwt()))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void create_user_with_user_role_jwt() throws Exception {
      mockMvc.perform(post(Endpoints.CREATE_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content("{}")
              .with(userRoleJwt()))
          .andExpect(status().isForbidden())
          .andExpect(content().json(DataLoader.loadFile("data/forbiddenResponse.json")));
    }

    @Test
    public void create_user_username_unavailable() throws Exception {
      JsonObject request = DataLoader.loadFileAsElement("data/user/createUserRequest.json").getAsJsonObject();
      request.addProperty("username", "testUser");

      mockMvc.perform(post(Endpoints.CREATE_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request))
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.BAD_REQUEST.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.BAD_REQUEST.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.user.username.unavailable")))
          .andExpect(jsonPath("$.moreInformation").value("testUser"));
    }

    @Test
    public void create_user_email_unavailable() throws Exception {
      JsonObject request = DataLoader.loadFileAsElement("data/user/createUserRequest.json").getAsJsonObject();
      request.addProperty("email", "test@email.com");

      mockMvc.perform(post(Endpoints.CREATE_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request))
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.BAD_REQUEST.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.BAD_REQUEST.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.user.email.unavailable")))
          .andExpect(jsonPath("$.moreInformation").value("test@email.com"));
    }

    @Test
    public void create_user_invalid_fields() throws Exception {
      JsonObject request = DataLoader.loadFileAsElement("data/user/createUserRequest.json").getAsJsonObject();
      request.addProperty("email", "a");
      request.addProperty("dob", "1/1/1");

      mockMvc.perform(post(Endpoints.CREATE_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request))
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.message").value(message("error.validation.body.default")))
          .andExpect(jsonPath("$.errors[?(@.name == 'dob' && @.provided == '1/1/1')]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'dob' && @.provided == '1/1/1')].messages[*]",
              arrayContaining(2, dateTimeFormatMessage("dd/MM/yyyy"), dateRangeMessage("01/01/1900", DateRange.NOW))))
          .andExpect(jsonPath("$.errors[?(@.name == 'email' && @.provided == 'a')]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'email' && @.provided == 'a')].messages[*]",
              arrayContaining(2, "error.validation.email.default", sizeMessage(3, 254))));
    }
  }

  @Nested
  @DisplayName("Update User Integration Tests")
  public class UpdateUserIT {
    @Test
    public void update_user_successfully() throws Exception {
      mockMvc.perform(patch(Endpoints.UPDATE_USER, "testUser")
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/updateUserRequest.json"))
              .with(jwt()))
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.email").value("testing@email.com"))
          .andExpect(jsonPath("$.username").value("testing"))
          .andExpect(jsonPath("$.firstName").value("Jon"))
          .andExpect(jsonPath("$.lastName").value("Dowe"))
          .andExpect(jsonPath("$.nickname").value(nullValue()))
          .andExpect(jsonPath("$.dob").value("12/01/1970"))
          .andExpect(jsonPath("$.createdTs").value(
              matchesRegex("\\d{2}/\\d{2}/\\d{4} \\d{2}:\\d{2}:\\d{2}")))
          .andExpect(jsonPath("$.updatedTs").value(
              matchesRegex("\\d{2}/\\d{2}/\\d{4} \\d{2}:\\d{2}:\\d{2}")));
    }

    @Test
    public void update_user_no_jwt() throws Exception {
      mockMvc.perform(patch(Endpoints.UPDATE_USER, "testUser")
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/updateUserRequest.json")))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void update_user_invalid_jwt() throws Exception {
      mockMvc.perform(patch(Endpoints.UPDATE_USER, "testUser")
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/updateUserRequest.json"))
              .with(invalidJwt()))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void update_user_with_user_role_jwt() throws Exception {
      mockMvc.perform(patch(Endpoints.UPDATE_USER, "testUser")
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/updateUserRequest.json"))
              .with(userRoleJwt()))
          .andExpect(status().isForbidden())
          .andExpect(content().json(DataLoader.loadFile("data/forbiddenResponse.json")));
    }

    @Test
    public void update_user_not_found() throws Exception {
      mockMvc.perform(patch(Endpoints.UPDATE_USER, "notfound")
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/user/updateUserRequest.json"))
              .with(jwt()))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.NOT_FOUND.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.NOT_FOUND.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.user.not-found")))
          .andExpect(jsonPath("$.moreInformation").value("notfound"));
    }

    @Test
    public void update_user_invalid_fields() throws Exception {
      JsonObject request = DataLoader.loadFileAsElement("data/user/updateUserRequest.json").getAsJsonObject();
      request.addProperty("email", "a");
      request.addProperty("dob", "1/1/1");

      mockMvc.perform(patch(Endpoints.UPDATE_USER, "notfound")
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request))
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.message").value(message("error.validation.body.default")))
          .andExpect(jsonPath("$.errors[?(@.name == 'dob' && @.provided == '1/1/1')]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'dob' && @.provided == '1/1/1')].messages[*]",
              arrayContaining(2, dateTimeFormatMessage("dd/MM/yyyy"), dateRangeMessage("01/01/1900", DateRange.NOW))))
          .andExpect(jsonPath("$.errors[?(@.name == 'email' && @.provided == 'a')]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'email' && @.provided == 'a')].messages[*]",
              arrayContaining(2, "error.validation.email.default", sizeMessage(3, 254))));
    }
  }

  @Nested
  @DisplayName("Get Session Integration Tests")
  public class GetSessionIT {
    @Test
    public void get_session_successfully() throws Exception {
      mockMvc.perform(get(Endpoints.SESSION)
              .queryParam("date", "01/01/2021")
              .with(jwt()))
          .andExpect(status().isOk())
          .andExpect(content().json(DataLoader.loadFile("data/session/sessionResponse.json")));
    }

    @Test
    public void get_session_no_jwt() throws Exception {
      mockMvc.perform(get(Endpoints.SESSION)
              .queryParam("date", "01/01/2021"))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void get_session_invalid_jwt() throws Exception {
      mockMvc.perform(get(Endpoints.SESSION)
              .queryParam("date", "01/01/2021")
              .with(invalidJwt()))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void get_session_not_found() throws Exception {
      mockMvc.perform(get(Endpoints.SESSION)
              .queryParam("date", "01/01/0001")
              .with(jwt()))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.NOT_FOUND.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.NOT_FOUND.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.session.not-found.date")))
          .andExpect(jsonPath("$.moreInformation").value("01/01/0001"));
    }

    @Test
    public void get_session_invalid_date() throws Exception {
      mockMvc.perform(get(Endpoints.SESSION)
              .queryParam("date", "invalid")
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.message").value(message("error.validation.param.default")))
          .andExpect(jsonPath("$.errors[?(@.name == 'date' && @.provided == 'invalid')]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'date' && @.provided == 'invalid')].messages[*]",
              arrayContaining(2, dateTimeFormatMessage("dd/MM/yyyy"), dateRangeMessage(DateRange.ANY, DateRange.ANY))));
    }
  }

  @Nested
  @DisplayName("Get Sessions Integration Tests")
  public class GetSessionsIT {
    @Test
    public void get_sessions_successfully() throws Exception {
      mockMvc.perform(get(Endpoints.SESSIONS)
              .queryParam("startDate", "01/01/2021")
              .queryParam("offset", "0")
              .queryParam("limit", "5")
              .with(jwt()))
          .andExpect(status().isOk())
          .andExpect(content().json(DataLoader.loadFile("data/session/sessionsResponse.json")));
    }

    @Test
    public void get_sessions_successfully_using_default_params() throws Exception {
      mockMvc.perform(get(Endpoints.SESSIONS)
              .with(jwt()))
          .andExpect(status().isOk())
          .andExpect(content().json(DataLoader.loadFile("data/session/sessionsResponse.json")));
    }

    @Test
    public void get_sessions_no_jwt() throws Exception {
      mockMvc.perform(get(Endpoints.SESSIONS))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void get_sessions_invalid_jwt() throws Exception {
      mockMvc.perform(get(Endpoints.SESSIONS)
              .with(invalidJwt()))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void get_sessions_not_found() throws Exception {
      mockMvc.perform(get(Endpoints.SESSIONS)
              .queryParam("startDate", "01/01/1970")
              .queryParam("offset", "0")
              .queryParam("limit", "5")
              .with(jwt()))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.NOT_FOUND.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.NOT_FOUND.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.sessions.not-found")))
          .andExpect(jsonPath("$.moreInformation", hasEntries(Map.of(
              "startDate", "01/01/1970",
              "offset", 0,
              "limit", 5))));
    }

    @Test
    public void get_sessions_invalid_params() throws Exception {
      mockMvc.perform(get(Endpoints.SESSIONS)
              .queryParam("startDate", "invalid")
              .queryParam("offset", "-1")
              .queryParam("limit", "0")
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.message").value(message("error.validation.param.default")))
          .andExpect(jsonPath("$.errors[?(@.name == 'startDate' && @.provided == 'invalid')]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'startDate' && @.provided == 'invalid')].messages[*]",
              arrayContaining(2, dateTimeFormatMessage( "dd/MM/yyyy"), dateRangeMessage(DateRange.ANY, DateRange.ANY))))
          .andExpect(jsonPath("$.errors[?(@.name == 'offset' && @.provided == -1)].messages[*]",
              arrayContaining(1, "must be greater than or equal to 0")))
          .andExpect(jsonPath("$.errors[?(@.name == 'limit' && @.provided == 0)].messages[*]",
              arrayContaining(1, "must be greater than or equal to 5")));
    }
  }

  @Nested
  @DisplayName("Create Sessions Integration Tests")
  public class CreateSessionsIT {
    @Test
    public void create_session_successfully() throws Exception {
      mockMvc.perform(post(Endpoints.SESSION)
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/session/createSessionRequest.json"))
              .with(jwt()))
          .andExpect(status().isCreated())
          .andExpect(jsonPath("$.id").value(matchesRegex("[a-f\\d]{8}(-[a-f\\d]{4}){3}-[a-f\\d]{12}")))
          .andExpect(content().json(DataLoader.loadFile("data/session/createSessionRequest.json")));
    }

    @Test
    public void create_session_no_jwt() throws Exception {
      mockMvc.perform(post(Endpoints.SESSION)
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/session/createSessionRequest.json")))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void create_session_invalid_jwt() throws Exception {
      mockMvc.perform(post(Endpoints.SESSION)
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/session/createSessionRequest.json"))
              .with(invalidJwt()))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void create_session_with_user_role_jwt() throws Exception {
      mockMvc.perform(post(Endpoints.SESSION)
              .contentType(MediaType.APPLICATION_JSON)
              .content(DataLoader.loadFile("data/session/createSessionRequest.json"))
              .with(userRoleJwt()))
          .andExpect(status().isForbidden())
          .andExpect(content().json(DataLoader.loadFile("data/forbiddenResponse.json")));
    }

    @Test
    public void create_session_invalid_fields() throws Exception {
      JsonObject request = DataLoader.loadFileAsElement("data/session/createSessionRequest.json").getAsJsonObject();
      request.addProperty("date", "invalid");
      request.getAsJsonArray("users")
          .get(0)
          .getAsJsonObject()
          .addProperty("inForPence", -1);

      mockMvc.perform(post(Endpoints.SESSION)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request))
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.message").value(message("error.validation.body.default")))
          .andExpect(jsonPath("$.errors[?(@.name == 'date' && @.provided == 'invalid')]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'date' && @.provided == 'invalid')].messages[*]",
              arrayContaining(2, dateTimeFormatMessage("dd/MM/yyyy"), dateRangeMessage(DateRange.ANY, DateRange.NOW))))
          .andExpect(jsonPath("$.errors[?(@.name == 'users[0].inForPence' && @.provided == -1)]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'users[0].inForPence' && @.provided == -1)].messages[*]",
              arrayContaining(1, "error.validation.min.int.negative")));
    }

    @Test
    public void create_session_user_not_found() throws Exception {
      JsonObject request = JsonUtils.fromJson(DataLoader.loadFile("data/session/createSessionRequest.json")).getAsJsonObject();
      request.getAsJsonArray("users")
          .get(0)
          .getAsJsonObject()
          .addProperty("username", "notfound");

      mockMvc.perform(post(Endpoints.SESSION)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request))
              .with(jwt()))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.NOT_FOUND.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.NOT_FOUND.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.user.not-found")))
          .andExpect(jsonPath("$.moreInformation").value("notfound"));
    }

    @Test
    public void create_session_total_out_for_too_high() throws Exception {
      JsonObject request = JsonUtils.fromJson(DataLoader.loadFile("data/session/createSessionRequest.json")).getAsJsonObject();
      request.getAsJsonArray("users")
          .get(0)
          .getAsJsonObject()
          .addProperty("outForPence", 10_000);

      mockMvc.perform(post(Endpoints.SESSION)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request))
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.BAD_REQUEST.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.BAD_REQUEST.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.user-data.out-for.extra")))
          .andExpect(jsonPath("$.moreInformation").value(greaterThan(0)));
    }

    @Test
    public void create_session_total_out_for_too_low() throws Exception {
      JsonObject request = JsonUtils.fromJson(DataLoader.loadFile("data/session/createSessionRequest.json")).getAsJsonObject();
      request.getAsJsonArray("users")
          .get(0)
          .getAsJsonObject()
          .addProperty("outForPence", 0);

      mockMvc.perform(post(Endpoints.SESSION)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request))
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.BAD_REQUEST.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.BAD_REQUEST.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.user-data.out-for.missing")))
          .andExpect(jsonPath("$.moreInformation").value(greaterThan(0)));
    }

    @Test
    public void create_session_duplicate_username() throws Exception {
      JsonObject request = JsonUtils.fromJson(DataLoader.loadFile("data/session/createSessionRequest.json")).getAsJsonObject();
      request.getAsJsonArray("users")
          .get(1)
          .getAsJsonObject()
          .addProperty("username", "testUser");

      mockMvc.perform(post(Endpoints.SESSION)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request))
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.BAD_REQUEST.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.BAD_REQUEST.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.user-data.username.duplicate")))
          .andExpect(jsonPath("$.moreInformation").value("testUser"));
    }
  }


  @Nested
  @DisplayName("Delete Sessions Integration Tests")
  public class DeleteSessionsIT {
    @Test
    public void delete_session_successfully() throws Exception {
      mockMvc.perform(delete(Endpoints.DELETE_SESSION, "4152e28b-3006-4e29-86e8-e4d991953c13")
              .with(jwt()))
          .andExpect(status().isOk())
          .andExpect(content().bytes(new byte[0]));
    }

    @Test
    public void delete_session_no_jwt() throws Exception {
      mockMvc.perform(delete(Endpoints.DELETE_SESSION, "4152e28b-3006-4e29-86e8-e4d991953c13"))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void delete_session_invalid_jwt() throws Exception {
      mockMvc.perform(delete(Endpoints.DELETE_SESSION, "4152e28b-3006-4e29-86e8-e4d991953c13")
              .with(invalidJwt()))
          .andExpect(status().isUnauthorized())
          .andExpect(content().json(DataLoader.loadFile("data/jwtUnauthorizedResponse.json")));
    }

    @Test
    public void delete_session_with_user_role_jwt() throws Exception {
      mockMvc.perform(delete(Endpoints.DELETE_SESSION, "4152e28b-3006-4e29-86e8-e4d991953c13")
              .with(userRoleJwt()))
          .andExpect(status().isForbidden())
          .andExpect(content().json(DataLoader.loadFile("data/forbiddenResponse.json")));
    }

    @Test
    public void delete_session_not_found() throws Exception {
      mockMvc.perform(delete(Endpoints.DELETE_SESSION, "4152e28b-3006-4e29-86e8-e4d991953caa")
              .with(jwt()))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.NOT_FOUND.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.NOT_FOUND.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.session.not-found.id")))
          .andExpect(jsonPath("$.moreInformation").value("4152e28b-3006-4e29-86e8-e4d991953caa"));
    }

    @Test
    public void delete_session_invalid_uuid() throws Exception {
      mockMvc.perform(delete(Endpoints.DELETE_SESSION, "invalid")
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.message").value(message("error.validation.param.default")))
          .andExpect(jsonPath("$.errors[?(@.name == 'id' && @.provided == 'invalid')]").exists())
          .andExpect(jsonPath("$.errors[?(@.name == 'id' && @.provided == 'invalid')].messages[*]",
              arrayContaining(1, "error.validation.uuid.default")));
    }
  }

  @Nested
  @DisplayName("Invalid Request Integration Tests")
  public class InvalidRequestIT {
    @Test
    public void no_endpoint_match() throws Exception {
      mockMvc.perform(post("/endpoint-that-does-not-exist")
            .contentType(MediaType.APPLICATION_JSON)
            .with(jwt()))
          .andExpect(status().isNotFound())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.NOT_FOUND.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.NOT_FOUND.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.validation.endpoint.not-found")))
          .andExpect(jsonPath("$.moreInformation").value("/endpoint-that-does-not-exist"));
    }

    @Test
    public void method_not_supported() throws Exception {
      mockMvc.perform(post(Endpoints.GET_USER, "testUser")
              .contentType(MediaType.APPLICATION_JSON)
              .with(jwt()))
          .andExpect(status().isMethodNotAllowed())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.METHOD_NOT_ALLOWED.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.METHOD_NOT_ALLOWED.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.validation.method.not-supported", "POST")))
          .andExpect(jsonPath("$.moreInformation").value(message("error.validation.method.supported", "GET, PATCH")));
    }

    @Test
    public void media_type_not_supported() throws Exception {
      mockMvc.perform(post(Endpoints.CREATE_USER)
              .contentType(MediaType.TEXT_PLAIN)
              .content("some text")
              .with(jwt()))
          .andExpect(status().isUnsupportedMediaType())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.UNSUPPORTED_MEDIA_TYPE.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.validation.media-type.not-supported")))
          .andExpect(jsonPath("$.moreInformation").value(MediaType.TEXT_PLAIN_VALUE));
    }

    @Test
    public void method_args_type_mismatch() throws Exception {
      mockMvc.perform(get(Endpoints.SESSIONS)
              .queryParam("offset", "mismatch")
              .contentType(MediaType.APPLICATION_JSON)
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.BAD_REQUEST.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.BAD_REQUEST.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.validation.method-args.mismatch", "offset", "Integer", "String")))
          .andExpect(jsonPath("$.moreInformation").value("mismatch"));
    }

    @Test
    public void invalid_json_request_body() throws Exception {
      mockMvc.perform(post(Endpoints.CREATE_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content("invalid json")
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.BAD_REQUEST.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.BAD_REQUEST.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.validation.body.parse-fail")))
          .andExpect(jsonPath("$.moreInformation").value(message("error.validation.body.invalid-json")));
    }

    @Test
    public void type_mismatch_in_json_request_body() throws Exception {
      JsonObject request = DataLoader.loadFileAsElement("data/user/createUserRequest.json").getAsJsonObject();
      JsonObject usernameInvalidType = new JsonObject();
      usernameInvalidType.addProperty("a", "b");
      request.add("username", usernameInvalidType);

      mockMvc.perform(post(Endpoints.CREATE_USER)
              .contentType(MediaType.APPLICATION_JSON)
              .content(JsonUtils.toJson(request))
              .with(jwt()))
          .andExpect(status().isBadRequest())
          .andExpect(jsonPath("$.httpStatus").value(HttpStatus.BAD_REQUEST.value()))
          .andExpect(jsonPath("$.error").value(HttpStatus.BAD_REQUEST.getReasonPhrase()))
          .andExpect(jsonPath("$.message").value(message("error.validation.body.parse-fail")))
          .andExpect(jsonPath("$.moreInformation").value("field 'username' expected String"));
    }
  }

  private RequestPostProcessor jwt() {
    return jwt(Function.identity());
  }

  private RequestPostProcessor jwt(Function<JwtBuilder, JwtBuilder> jwtBuilderFunc) {
    return request -> {
      request.addHeader(HttpHeaders.AUTHORIZATION,
          "Bearer " + jwtBuilderFunc.apply(defaultJwt()).compact());
      return request;
    };
  }

  private RequestPostProcessor invalidJwt() {
    return request -> {
      request.addHeader(HttpHeaders.AUTHORIZATION, "invalid-token");
      return request;
    };
  }

  private RequestPostProcessor userRoleJwt() {
    return jwt(jwtBuilder -> jwtBuilder
        .setSubject("e85b3a36-3c6b-4c81-8e59-847e56099d72")
        .addClaims(Map.of("roles", Set.of("ROLE_USER"))));
  }

  private JwtBuilder defaultJwt() {
    return Jwts.builder()
        .addClaims(Map.of("roles", Set.of("ROLE_OWNER", "ROLE_USER")))
        .setSubject("145a3631-70eb-47f6-b70d-e6f83557f8da")
        .signWith(SignatureAlgorithm.HS512, jwtSecret.getBytes(StandardCharsets.UTF_8));
  }

  private static Matcher<Collection<String>> arrayContaining(int size, String... messages) {
    return arrayContaining(size, Arrays.stream(messages)
        .map(MessageSourceInterpolator::message)
        .map(Matchers::equalTo)
        .collect(Collectors.toList()));
  }

  @SafeVarargs
  private static <T> Matcher<Collection<T>> arrayContaining(int size, T... messages) {
    return arrayContaining(size, Arrays.stream(messages)
        .map(Matchers::equalTo)
        .collect(Collectors.toList()));
  }

  private static <T> Matcher<Collection<T>> arrayContaining(int size, Collection<Matcher<? super T>> messages) {
    return Matchers.<Collection<T>>allOf(
        hasSize(size),
        containsInAnyOrder(messages));
  }

  private static Matcher<?> hasEntries(Map<String, Object> map) {
    return allOf(map.entrySet()
        .stream()
        .map(e -> hasEntry(e.getKey(), e.getValue()))
        .collect(Collectors.toList()));
  }
}