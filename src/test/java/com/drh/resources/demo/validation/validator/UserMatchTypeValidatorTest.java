package com.drh.resources.demo.validation.validator;

import static org.assertj.core.api.Assertions.assertThat;

import com.drh.resources.demo.support.AnnotationCreator;
import com.drh.resources.demo.support.MockitoUnitTest;
import com.drh.resources.demo.validation.validator.annotation.UserMatchType;
import javax.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

@DisplayName("User Match Type Validator Unit Tests")
public class UserMatchTypeValidatorTest extends MockitoUnitTest {

  @Mock
  private ConstraintValidatorContext context;

  private UserMatchTypeValidator validator;

  @BeforeEach
  public void beforeEach() {
    validator = new UserMatchTypeValidator();
  }

  @Test
  public void is_valid_default() {
    validator.initialize(AnnotationCreator.create(UserMatchType.class));

    assertThat(validator.isValid("email", context)).isTrue();
    assertThat(validator.isValid("username", context)).isTrue();

    assertThat(validator.isValid("", context)).isFalse();
    assertThat(validator.isValid(null, context)).isFalse();
    assertThat(validator.isValid("invalid", context)).isFalse();
  }

}
