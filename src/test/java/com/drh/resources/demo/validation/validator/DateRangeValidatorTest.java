package com.drh.resources.demo.validation.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import com.drh.resources.demo.support.AnnotationCreator;
import com.drh.resources.demo.support.MockitoUnitTest;
import com.drh.resources.demo.validation.validator.annotation.DateRange;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.validation.ConstraintValidatorContext;
import org.hibernate.validator.constraintvalidation.HibernateConstraintValidatorContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

@DisplayName("Date Range Validator Unit Tests")
public class DateRangeValidatorTest extends MockitoUnitTest {

  @Mock
  private ConstraintValidatorContext context;

  @Mock
  private HibernateConstraintValidatorContext hibernateContext;

  private DateRangeValidator validator;

  @BeforeEach
  public void beforeEach() {
    validator = new DateRangeValidator();
  }

  @Test
  public void is_valid_default() {
    validator.initialize(AnnotationCreator.create(DateRange.class));

    when(context.unwrap(eq(HibernateConstraintValidatorContext.class))).thenReturn(hibernateContext);
    when(hibernateContext.addExpressionVariable(anyString(), any())).thenReturn(hibernateContext);

    assertThat(validator.isValid("01/01/1900", context)).isTrue();
    assertThat(validator.isValid("01/01/0001", context)).isTrue();
    assertThat(validator.isValid("31/12/9999", context)).isTrue();
    assertThat(validator.isValid(null, context)).isTrue();

    assertThat(validator.isValid("01/01/0000", context)).isFalse();
    assertThat(validator.isValid("invalid", context)).isFalse();
    assertThat(validator.isValid("", context)).isFalse();
  }

  @Test
  public void is_valid_from_epoch() {
    validator.initialize(AnnotationCreator.of(DateRange.class)
        .value("from", DateRange.EPOCH)
        .create());

    when(context.unwrap(eq(HibernateConstraintValidatorContext.class))).thenReturn(hibernateContext);
    when(hibernateContext.addExpressionVariable(anyString(), any())).thenReturn(hibernateContext);

    assertThat(validator.isValid("01/01/2021", context)).isTrue();
    assertThat(validator.isValid("01/01/1970", context)).isTrue();
    assertThat(validator.isValid("31/12/9999", context)).isTrue();
    assertThat(validator.isValid(null, context)).isTrue();

    assertThat(validator.isValid("01/01/0001", context)).isFalse();
    assertThat(validator.isValid("01/01/0000", context)).isFalse();
    assertThat(validator.isValid("invalid", context)).isFalse();
    assertThat(validator.isValid("", context)).isFalse();
  }

  @Test
  public void is_valid_from_now() {
    String now = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(LocalDate.now());

    validator.initialize(AnnotationCreator.of(DateRange.class)
        .value("from", DateRange.NOW)
        .create());

    when(context.unwrap(eq(HibernateConstraintValidatorContext.class))).thenReturn(hibernateContext);
    when(hibernateContext.addExpressionVariable(anyString(), any())).thenReturn(hibernateContext);

    assertThat(validator.isValid(now, context)).isTrue();
    assertThat(validator.isValid("31/12/9999", context)).isTrue();
    assertThat(validator.isValid(null, context)).isTrue();

    assertThat(validator.isValid("01/01/2021", context)).isFalse();
    assertThat(validator.isValid("01/01/0000", context)).isFalse();
    assertThat(validator.isValid("invalid", context)).isFalse();
    assertThat(validator.isValid("", context)).isFalse();
  }

  @Test
  public void is_valid_from_defined() {
    validator.initialize(AnnotationCreator.of(DateRange.class)
        .value("from", "01/01/2021")
        .create());

    when(context.unwrap(eq(HibernateConstraintValidatorContext.class))).thenReturn(hibernateContext);
    when(hibernateContext.addExpressionVariable(anyString(), any())).thenReturn(hibernateContext);

    assertThat(validator.isValid("31/12/9999", context)).isTrue();
    assertThat(validator.isValid("01/01/2021", context)).isTrue();
    assertThat(validator.isValid(null, context)).isTrue();

    assertThat(validator.isValid("01/01/0001", context)).isFalse();
    assertThat(validator.isValid("01/01/0000", context)).isFalse();
    assertThat(validator.isValid("invalid", context)).isFalse();
    assertThat(validator.isValid("", context)).isFalse();
  }

  @Test
  public void is_valid_to_epoch() {
    validator.initialize(AnnotationCreator.of(DateRange.class)
        .value("to", DateRange.EPOCH)
        .create());

    when(context.unwrap(eq(HibernateConstraintValidatorContext.class))).thenReturn(hibernateContext);
    when(hibernateContext.addExpressionVariable(anyString(), any())).thenReturn(hibernateContext);

    assertThat(validator.isValid("01/01/0001", context)).isTrue();
    assertThat(validator.isValid("01/01/1970", context)).isTrue();
    assertThat(validator.isValid(null, context)).isTrue();

    assertThat(validator.isValid("01/01/2021", context)).isFalse();
    assertThat(validator.isValid("31/12/9999", context)).isFalse();
    assertThat(validator.isValid("01/01/0000", context)).isFalse();
    assertThat(validator.isValid("invalid", context)).isFalse();
    assertThat(validator.isValid("", context)).isFalse();
  }

  @Test
  public void is_valid_to_now() {
    String now = DateTimeFormatter.ofPattern("dd/MM/yyyy").format(LocalDate.now());

    validator.initialize(AnnotationCreator.of(DateRange.class)
        .value("to", DateRange.NOW)
        .create());

    when(context.unwrap(eq(HibernateConstraintValidatorContext.class))).thenReturn(hibernateContext);
    when(hibernateContext.addExpressionVariable(anyString(), any())).thenReturn(hibernateContext);

    assertThat(validator.isValid(now, context)).isTrue();
    assertThat(validator.isValid("01/01/2021", context)).isTrue();
    assertThat(validator.isValid(null, context)).isTrue();

    assertThat(validator.isValid("31/12/9999", context)).isFalse();
    assertThat(validator.isValid("01/01/0000", context)).isFalse();
    assertThat(validator.isValid("invalid", context)).isFalse();
    assertThat(validator.isValid("", context)).isFalse();
  }

  @Test
  public void is_valid_to_defined() {
    validator.initialize(AnnotationCreator.of(DateRange.class)
        .value("to", "01/01/2021")
        .create());

    when(context.unwrap(eq(HibernateConstraintValidatorContext.class))).thenReturn(hibernateContext);
    when(hibernateContext.addExpressionVariable(anyString(), any())).thenReturn(hibernateContext);

    assertThat(validator.isValid("01/01/0001", context)).isTrue();
    assertThat(validator.isValid("01/01/2021", context)).isTrue();
    assertThat(validator.isValid(null, context)).isTrue();

    assertThat(validator.isValid("31/12/9999", context)).isFalse();
    assertThat(validator.isValid("01/01/0000", context)).isFalse();
    assertThat(validator.isValid("invalid", context)).isFalse();
    assertThat(validator.isValid("", context)).isFalse();
  }

  @Test
  public void is_valid_to_less_than_from() {
    DateRange dateRange = AnnotationCreator.of(DateRange.class)
        .value("from", "02/01/2021")
        .value("to", "01/01/2021")
        .create();

    IllegalArgumentException error = assertThrows(IllegalArgumentException.class,
        () -> validator.initialize(dateRange));

    assertThat(error.getMessage()).isEqualTo("to '01/01/2021' is before from '02/01/2021'");
  }
}
