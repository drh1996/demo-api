package com.drh.resources.demo.validation.factory;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.drh.resources.demo.exception.ParameterValidationException;
import com.drh.resources.demo.persistance.dto.UserDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@DisplayName("Validator Bean Integration Tests")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class CustomLocalValidatorFactoryBeanIT {

  @Autowired
  private CustomLocalValidatorFactoryBean validator;

  @Test
  public void validate_param_successfully() {
    assertDoesNotThrow(() ->
        validator.validateParam(UserDto.class, "username", "testUser", "identifier"));
  }

  @Test
  public void validate_param_failures() {
    ParameterValidationException sizeViolation = assertThrows(ParameterValidationException.class, () ->
        validator.validateParam(UserDto.class, "username", "a", "identifier"));
    paramViolationAssert(sizeViolation, "a", "identifier");

    ParameterValidationException sizeViolationNoParamName = assertThrows(ParameterValidationException.class, () ->
        validator.validateParam(UserDto.class, "username", "a", null));
    paramViolationAssert(sizeViolationNoParamName, "a", "username");

    ParameterValidationException nullViolation = assertThrows(ParameterValidationException.class, () ->
        validator.validateParam(UserDto.class, "username", null, "identifier"));
    paramViolationAssert(nullViolation, null, "identifier");
  }

  private void paramViolationAssert(ParameterValidationException e, Object target, String paramName) {
    assertThat(e.getTarget()).usingRecursiveComparison().isEqualTo(target);
    assertThat(e.getParamName()).isEqualTo(paramName);
  }

}
