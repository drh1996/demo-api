package com.drh.resources.demo.validation.validator;

import static org.assertj.core.api.Assertions.assertThat;

import com.drh.resources.demo.support.AnnotationCreator;
import com.drh.resources.demo.support.MockitoUnitTest;
import com.drh.resources.demo.validation.validator.annotation.Uuid;
import java.util.UUID;
import javax.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

@DisplayName("UUID Validator Unit Tests")
public class UuidValidatorTest extends MockitoUnitTest {

  @Mock
  private ConstraintValidatorContext context;

  private UuidValidator validator;

  @BeforeEach
  public void beforeEach() {
    validator = new UuidValidator();
  }

  @Test
  public void is_valid_default() {
    validator.initialize(AnnotationCreator.create(Uuid.class));

    assertThat(validator.isValid(UUID.randomUUID().toString(), context)).isTrue();

    assertThat(validator.isValid("invalid", context)).isFalse();
    assertThat(validator.isValid("", context)).isFalse();
    assertThat(validator.isValid(null, context)).isFalse();
  }
}
