package com.drh.resources.demo.validation.validator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import com.drh.resources.demo.support.AnnotationCreator;
import com.drh.resources.demo.support.MockitoUnitTest;
import com.drh.resources.demo.validation.validator.annotation.Password;
import javax.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.LengthRule;
import org.passay.WhitespaceRule;
import org.passay.spring.SpringMessageResolver;

@DisplayName("Password Validator Unit Tests")
public class PasswordValidatorTest extends MockitoUnitTest {

  @Mock
  private ConstraintValidatorContext context;

  @Mock
  private ConstraintValidatorContext.ConstraintViolationBuilder violationBuilder;

  private PasswordValidator validator;

  @BeforeEach
  public void beforeEach() {
    validator = new PasswordValidator(new org.passay.PasswordValidator(
        new SpringMessageResolver(messageSource),
        new LengthRule(8, 20),
        new CharacterRule(EnglishCharacterData.UpperCase, 1),
        new CharacterRule(EnglishCharacterData.LowerCase, 1),
        new CharacterRule(EnglishCharacterData.Digit, 1),
        new CharacterRule(EnglishCharacterData.Special, 1),
        new WhitespaceRule()));
  }

  @Test
  public void is_valid_default() {
    validator.initialize(AnnotationCreator.create(Password.class));

    doNothing().when(context).disableDefaultConstraintViolation();
    when(context.buildConstraintViolationWithTemplate(anyString())).thenReturn(violationBuilder);
    when(violationBuilder.addConstraintViolation()).thenReturn(context);

    assertThat(validator.isValid("a8(DefgH", context)).isTrue();
    assertThat(validator.isValid(null, context)).isTrue();

    assertThat(validator.isValid("password", context)).isFalse();
    assertThat(validator.isValid("123", context)).isFalse();
    assertThat(validator.isValid("!Ab8-----------------", context)).isFalse();
  }
}
