package com.drh.resources.demo.validation.configuration;

import static org.assertj.core.api.Assertions.assertThat;

import com.drh.resources.demo.support.MockitoUnitTest;
import java.util.Locale;
import javax.validation.MessageInterpolator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

@DisplayName("Message Interpolator Unit Tests")
public class MessageSourceMessageInterpolatorTest extends MockitoUnitTest {

  private MessageSourceMessageInterpolator interpolator;

  @Mock
  private MessageInterpolator.Context context;

  @BeforeEach
  public void beforeEach() {
    interpolator = new MessageSourceMessageInterpolator(messageSource);
  }

  @Test
  public void interpolate_successfully() {
    String result = interpolator.interpolate("error.validation.null.default", context, Locale.UK);
    assertThat(result).isNotNull().isNotEmpty();
  }

  @Test
  public void interpolate_no_locale_successfully() {
    String result = interpolator.interpolate("error.validation.null.default", context);
    assertThat(result).isNotNull().isNotEmpty();
  }
}
