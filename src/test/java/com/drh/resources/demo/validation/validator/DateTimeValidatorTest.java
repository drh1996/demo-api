package com.drh.resources.demo.validation.validator;

import static org.assertj.core.api.Assertions.assertThat;

import com.drh.resources.demo.support.AnnotationCreator;
import com.drh.resources.demo.support.MockitoUnitTest;
import com.drh.resources.demo.validation.validator.annotation.DateTimeFormat;
import javax.validation.ConstraintValidatorContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

@DisplayName("Date Time Validator Unit Tests")
public class DateTimeValidatorTest extends MockitoUnitTest {

  @Mock
  private ConstraintValidatorContext context;

  private DateTimeValidator validator;

  @BeforeEach
  public void beforeEach() {
    validator = new DateTimeValidator();
  }

  @Test
  public void is_valid_default() {
    validator.initialize(AnnotationCreator.create(DateTimeFormat.class));

    assertThat(validator.isValid("01/01/0001", context)).isTrue();
    assertThat(validator.isValid("01/01/0001 12:12:12", context)).isTrue();
    assertThat(validator.isValid("", context)).isTrue();
    assertThat(validator.isValid(null, context)).isTrue();

    assertThat(validator.isValid("1/1/1", context)).isFalse();
    assertThat(validator.isValid("invalid", context)).isFalse();
  }

  @Test
  public void is_valid_date_only_format() {
    validator.initialize(AnnotationCreator.of(DateTimeFormat.class)
        .value("format", "dd/MM/yyyy")
        .create());

    assertThat(validator.isValid("01/01/0001", context)).isTrue();
    assertThat(validator.isValid("", context)).isTrue();
    assertThat(validator.isValid(null, context)).isTrue();

    assertThat(validator.isValid("01/01/0001 12:12:12", context)).isFalse();
    assertThat(validator.isValid("1/1/1", context)).isFalse();
    assertThat(validator.isValid("invalid", context)).isFalse();
  }

  @Test
  public void is_valid_dont_allow_empty() {
    validator.initialize(AnnotationCreator.of(DateTimeFormat.class)
        .value("allowEmpty", false)
        .create());

    assertThat(validator.isValid("01/01/0001", context)).isTrue();
    assertThat(validator.isValid("01/01/0001 12:12:12", context)).isTrue();

    assertThat(validator.isValid("", context)).isFalse();
    assertThat(validator.isValid(null, context)).isFalse();
    assertThat(validator.isValid("1/1/1", context)).isFalse();
    assertThat(validator.isValid("invalid", context)).isFalse();
  }
}
