package com.drh.resources.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.drh.resources.demo.model.UserMatchTypeEnum;
import com.drh.resources.demo.persistance.dto.SessionDto;
import com.drh.resources.demo.persistance.dto.UpdateUserDto;
import com.drh.resources.demo.persistance.dto.UserDto;
import com.drh.resources.demo.service.DemoService;
import com.drh.resources.demo.support.DataLoader;
import com.drh.resources.demo.support.MockitoUnitTest;
import com.drh.resources.demo.validation.factory.CustomLocalValidatorFactoryBean;
import com.google.gson.reflect.TypeToken;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;

@DisplayName("Controller Unit Tests")
public class DemoControllerTest extends MockitoUnitTest {

  @Mock
  private DemoService demoService;

  @Mock
  private CustomLocalValidatorFactoryBean customLocalValidatorFactoryBean;

  @Mock
  private ModelMapper modelMapper;

  private DemoController demoController;

  @BeforeEach
  public void beforeEach() {
    demoController = new DemoController(demoService, customLocalValidatorFactoryBean, modelMapper);
  }

  @AfterEach
  public void afterEach() {
    verifyNoMoreInteractions(demoService, customLocalValidatorFactoryBean, modelMapper);
  }

  @Test
  public void get_user_successfully() {
    UserDto expected = DataLoader.loadFile("data/user/createUserResponse.json", UserDto.class);

    doNothing().when(customLocalValidatorFactoryBean).validateParam(eq(UserDto.class), eq("email"), eq("test@email.com"), eq("identifier"));
    when(demoService.getUser(anyString(), any(UserMatchTypeEnum.class)))
        .thenReturn(expected);

    UserDto actual = demoController.getUser("test@email.com", "email");

    ArgumentCaptor<String> identifierCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<UserMatchTypeEnum> matchTypeCaptor = ArgumentCaptor.forClass(UserMatchTypeEnum.class);
    verify(customLocalValidatorFactoryBean).validateParam(eq(UserDto.class), eq("email"), eq("test@email.com"), eq("identifier"));
    verify(demoService).getUser(identifierCaptor.capture(), matchTypeCaptor.capture());

    assertThat(identifierCaptor.getValue()).isEqualTo("test@email.com");
    assertThat(matchTypeCaptor.getValue()).isEqualTo(UserMatchTypeEnum.EMAIL);

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void create_user_successfully() {
    UserDto expected = DataLoader.loadFile("data/user/createUserResponse.json", UserDto.class);
    UserDto request = DataLoader.loadFile("data/user/createUserRequest.json", UserDto.class);

    when(demoService.createUser(eq(request))).thenReturn(expected);

    UserDto actual = demoController.createUser(request);

    verify(demoService).createUser(eq(request));

    assertThat(actual)
        .usingRecursiveComparison()
        .ignoringFields("createdTs")
        .isEqualTo(expected);
  }

  @Test
  public void update_user_successfully() {
    UserDto expected = DataLoader.loadFile("data/user/updateUserResponse.json", UserDto.class);
    UpdateUserDto request = DataLoader.loadFile("data/user/updateUserRequest.json", UpdateUserDto.class);

    when(demoService.updateUser(eq("test"), eq(request))).thenReturn(expected);

    UserDto actual = demoController.updateUser("test", request);

    verify(demoService).updateUser(eq("test"), eq(request));

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void get_session_successfully() {
    SessionDto expected = DataLoader.loadFile("data/session/sessionResponse.json", SessionDto.class, true);

    when(modelMapper.map(anyString(), eq(LocalDate.class))).thenReturn(LocalDate.of(2021, 1, 1));
    when(demoService.getSession(any(LocalDate.class))).thenReturn(expected);

    SessionDto actual = demoController.getSession("01/01/2021");

    verify(demoService).getSession(any(LocalDate.class));

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void get_sessions_successfully() {
    List<SessionDto> expected = DataLoader.loadFile("data/session/sessionsResponse.json", new TypeToken<List<SessionDto>>(){}.getType());

    when(demoService.getSessions(any(LocalDate.class), anyInt(), anyInt())).thenReturn(expected);

    List<SessionDto> actual = demoController.getSessionsByDate(null, 0, 5);

    verify(demoService).getSessions(any(LocalDate.class), anyInt(), anyInt());

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void create_session_successfully() {
    SessionDto request = DataLoader.loadFile("data/session/createSessionRequest.json", SessionDto.class, true);

    SessionDto expected = DataLoader.loadFile("data/session/createSessionRequest.json", SessionDto.class, true);
    expected.setId(UUID.randomUUID());

    when(demoService.createSession(any(SessionDto.class))).thenReturn(expected);

    SessionDto actual = demoController.createSession(request);

    verify(demoService).createSession(any(SessionDto.class));

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void delete_session_successfully() {
    UUID requestUuid = UUID.randomUUID();

    doNothing().when(demoService).deleteSession(any(UUID.class));

    assertDoesNotThrow(() -> demoController.deleteSession(requestUuid.toString()));

    verify(demoService).deleteSession(any(UUID.class));
  }
}
