package com.drh.resources.demo.support;

import com.drh.resources.demo.persistance.dto.Nullable;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JsonUtils {

  private static final Gson GSON = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
  private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  static {
    setupObjectMapper();
  }

  private static void setupObjectMapper() {
    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

    SimpleModule m = new SimpleModule("testing only module");
    m.addDeserializer(LocalDate.class, new JsonDeserializer<>() {
      @Override
      public LocalDate deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return LocalDate.parse(p.getText(), dateFormat);
      }
    });
    m.addDeserializer(LocalDateTime.class, new JsonDeserializer<>() {
      @Override
      public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        return LocalDateTime.parse(p.getText(), dateTimeFormat);
      }
    });
    m.addSerializer(LocalDate.class, new JsonSerializer<>() {
      @Override
      public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider serializers)
          throws IOException {
        gen.writeString(dateFormat.format(value));
      }
    });
    m.addSerializer(LocalDateTime.class, new JsonSerializer<>() {
      @Override
      public void serialize(LocalDateTime value, JsonGenerator gen, SerializerProvider serializers)
          throws IOException {
        gen.writeString(dateTimeFormat.format(value));
      }
    });
    m.addSerializer(Nullable.class, new JsonSerializer<>() {
      @Override
      public void serialize(Nullable value, JsonGenerator gen, SerializerProvider serializers)
          throws IOException {
        Object obj = value.get();
        if (obj == null) {
          gen.writeNull();
        } else {
          gen.writeObject(obj);
        }
      }
    });

    OBJECT_MAPPER.registerModule(m);
  }

  public static String toJson(JsonElement jsonElement) {
    return GSON.toJson(jsonElement);
  }

  public static String toJson(Object obj) throws JsonProcessingException {
    return toJson(obj, false);
  }

  public static String toJson(Object obj, boolean ignoreAnnotations) throws JsonProcessingException {
    if (ignoreAnnotations) {
      return GSON.toJson(obj);
    }
    return OBJECT_MAPPER.writeValueAsString(obj);
  }

  public static JsonElement fromJson(String json) {
    return com.google.gson.JsonParser.parseString(json);
  }

  public static <T> T fromJson(String json, Class<T> clazz) {
    return fromJson(json, clazz, false);
  }

  public static <T> T fromJson(String json, Class<T> clazz, boolean ignoreAnnotations) {
    if (ignoreAnnotations) {
      return GSON.fromJson(json, clazz);
    } else {
      try {
        return OBJECT_MAPPER.readValue(json, clazz);
      } catch (JsonProcessingException e) {
        throw new JsonParseException(e);
      }
    }
  }

  public static <T> T fromJson(String json, TypeReference<T> typeRef) {
    try {
      return OBJECT_MAPPER.readValue(json, typeRef);
    } catch (JsonProcessingException e) {
      throw new JsonParseException(e);
    }
  }

  public static <T> T fromJson(String json, Type typeRef) {
    return GSON.fromJson(json, typeRef);
  }
}
