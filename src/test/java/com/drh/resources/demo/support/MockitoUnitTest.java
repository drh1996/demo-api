package com.drh.resources.demo.support;

import java.util.Locale;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@ExtendWith(MockitoExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class MockitoUnitTest {

  protected ReloadableResourceBundleMessageSource messageSource;

  @BeforeEach
  private void __beforeEachInternal__() {
    this.messageSource = createMessageSource();
  }

  protected ReloadableResourceBundleMessageSource createMessageSource() {
    ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setCacheMillis(-1);
    messageSource.setDefaultEncoding("UTF-8");
    messageSource.setBasename("classpath:/locale/messages");
    return messageSource;
  }

  protected String getMessage(String messageCode, Object... args) {
    return messageSource.getMessage(messageCode, args, messageCode, Locale.getDefault());
  }

}
