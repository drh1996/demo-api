package com.drh.resources.demo.support;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.JsonElement;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

public class DataLoader {

  private static final Map<String, String> FILE_CACHE = new HashMap<>();

  public static String loadFile(String filepath) {
    if (FILE_CACHE.containsKey(filepath)) {
      return FILE_CACHE.get(filepath);
    }

    String contents = loadFromInputStream(filepath);
    FILE_CACHE.put(filepath, contents);
    return contents;
  }

  public static <T> T loadFile(String filepath, Class<T> clazz) {
    return loadFile(filepath, clazz, false);
  }

  public static <T> T loadFile(String filepath, Class<T> clazz, boolean ignoreAnnotations) {
    return JsonUtils.fromJson(loadFile(filepath), clazz, ignoreAnnotations);
  }

  public static <T> T loadFile(String filepath, TypeReference<T> typeRef) {
    return JsonUtils.fromJson(loadFile(filepath), typeRef);
  }

  public static <T> T loadFile(String filepath, Type typeRef) {
    return JsonUtils.fromJson(loadFile(filepath), typeRef);
  }

  public static JsonElement loadFileAsElement(String filename) {
    return JsonUtils.fromJson(loadFile(filename));
  }

  private static String loadFromInputStream(String filepath) {
    InputStream is = DataLoader.class.getClassLoader().getResourceAsStream(filepath);
    if (is == null) {
      throw new FileLoadingException(filepath);
    }

    int readBytes;
    byte[] bytes = new byte[2048];
    StringBuilder sb = new StringBuilder();
    try {
      while ((readBytes = is.read(bytes, 0, 1024)) >= 0) {
        sb.append(new String(bytes, 0, readBytes));
      }
    } catch (IOException e) {
      throw new FileLoadingException(filepath, e);
    }
    return sb.toString();
  }

}
