package com.drh.resources.demo.support;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.Map;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.common.annotationfactory.AnnotationDescriptor;
import org.hibernate.annotations.common.annotationfactory.AnnotationFactory;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class AnnotationCreator {

  private final Class<? extends Annotation> annotationClass;
  private final Map<String, Object> values;

  public AnnotationCreator value(String elementName, Object value) {
    values.put(elementName, value);
    return this;
  }

  public <T extends Annotation> T create() {
    AnnotationDescriptor descriptor = new AnnotationDescriptor(annotationClass);
    values.forEach(descriptor::setValue);
    return AnnotationFactory.create(descriptor);
  }

  public static AnnotationCreator of(Class<? extends Annotation> annotationClass) {
    return new AnnotationCreator(annotationClass, new HashMap<>());
  }

  public static <T extends Annotation> T create(Class<? extends Annotation> annotationClass) {
    return new AnnotationCreator(annotationClass, new HashMap<>()).create();
  }

}
