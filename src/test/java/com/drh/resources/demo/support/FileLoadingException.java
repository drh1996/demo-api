package com.drh.resources.demo.support;

public class FileLoadingException extends RuntimeException {

  public FileLoadingException(String filepath, Throwable cause) {
    super("Failed to load file '" + filepath + "'", cause);
  }

  public FileLoadingException(String filepath) {
    super("Failed to load file '" + filepath + "'");
  }

}
