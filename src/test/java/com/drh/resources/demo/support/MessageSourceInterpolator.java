package com.drh.resources.demo.support;

import com.drh.resources.demo.validation.validator.annotation.DateRange;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Component;

@Component
@Profile("test")
public class MessageSourceInterpolator {

  private static ReloadableResourceBundleMessageSource messageSource;
  private static DateTimeFormatter dateFormat;

  @Autowired
  private void setMessageSource(ReloadableResourceBundleMessageSource messageSource) {
    MessageSourceInterpolator.messageSource = messageSource;
  }

  @Value("${consts.date-format}")
  private void setDateFormat(String dateFormat) {
    MessageSourceInterpolator.dateFormat = DateTimeFormatter.ofPattern(dateFormat);
  }

  public static Map<String, String> size(int min, int max) {
    return Map.of("{min}", String.valueOf(min), "{max}", String.valueOf(max));
  }

  public static String sizeMessage(int min, int max, Object... args) {
    return sizeMessage("error.validation.size.string.default", min, max, args);
  }

  public static String sizeMessage(String messageTemplate, int min, int max, Object... args) {
    return message(messageTemplate, size(min, max), args);
  }

  public static Map<String, String> dateRange(String fromParsed, String toParsed) {
    return Map.of(
        "${fromParsed}", parseDateRangeVal(fromParsed, true),
        "${toParsed}", parseDateRangeVal(toParsed, false));
  }

  public static String dateRangeMessage(String fromParsed, String toParsed, Object... args) {
    return dateRangeMessage("error.validation.date-time.range.default", fromParsed, toParsed, args);
  }

  public static String dateRangeMessage(String messageTemplate, String fromParsed, String toParsed, Object... args) {
    return message(messageTemplate, dateRange(fromParsed, toParsed), args);
  }

  public static Map<String, String> dateTimeFormat(String format) {
    return Map.of("{format}", format);
  }

  public static String dateTimeFormatMessage(String format, Object... args) {
    return dateTimeFormatMessage("error.validation.date-time.format.default", format, args);
  }

  public static String dateTimeFormatMessage(String messageTemplate, String format, Object... args) {
    return message(messageTemplate, dateTimeFormat(format), args);
  }

  public static String message(String messageCode, Map<String, String> interpolateMap, Object... args) {
    return interpolate(message(messageCode, args), interpolateMap);
  }

  public static String message(String messageCode, Object... args) {
    return messageSource.getMessage(messageCode, args, messageCode, Locale.UK);
  }

  public static String getCurrentDate() {
    return dateFormat.format(LocalDate.now());
  }

  private static String interpolate(String message, Map<String, String> interpolateMap) {
    String result = message;
    for (Entry<String, String> entry : interpolateMap.entrySet()) {
      int index;
      if ((index = result.indexOf(entry.getKey())) != -1) {
        result = result.substring(0, index) + entry.getValue() + result.substring(index + entry.getKey().length());
      }
    }
    return result;
  }

  private static String parseDateRangeVal(String val, boolean isFrom) {
    if (val.equals(DateRange.NOW)) {
      return getCurrentDate();
    }

    if (val.equals(DateRange.ANY)) {
      return isFrom ? "01/01/0001" : "31/12/9999";
    }

    if (val.equals(DateRange.EPOCH)) {
      return "01/01/1970";
    }

    return val;
  }
}
