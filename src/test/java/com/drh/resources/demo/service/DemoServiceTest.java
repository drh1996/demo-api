package com.drh.resources.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.drh.resources.demo.model.UserMatchTypeEnum;
import com.drh.resources.demo.persistance.dao.UserDao;
import com.drh.resources.demo.persistance.dto.SessionDto;
import com.drh.resources.demo.persistance.dto.UpdateUserDto;
import com.drh.resources.demo.persistance.dto.UserDto;
import com.drh.resources.demo.service.jwt.service.JwtService;
import com.drh.resources.demo.service.session.SessionService;
import com.drh.resources.demo.service.user.UserService;
import com.drh.resources.demo.support.DataLoader;
import com.drh.resources.demo.support.MockitoUnitTest;
import com.google.gson.reflect.TypeToken;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;

@DisplayName("Demo Service Unit Tests")
public class DemoServiceTest extends MockitoUnitTest {

  @Mock
  private UserService userService;

  @Mock
  private SessionService sessionService;

  @Mock
  private JwtService jwtService;

  @Mock
  private PasswordEncoder passwordEncoder;

  private DemoService demoService;

  @BeforeEach
  public void beforeEach() {
    demoService = new DemoServiceImpl(userService, sessionService, jwtService, passwordEncoder);
  }

  @AfterEach
  public void afterEach() {
    verifyNoMoreInteractions(userService, sessionService);
  }

  @Test
  public void get_user_successfully() {
    UserDto expected = DataLoader.loadFile("data/user/createUserResponse.json", UserDto.class, true);

    when(userService.getUser(anyString(), any(UserMatchTypeEnum.class))).thenReturn(expected);

    UserDto actual = demoService.getUser("test@email.com", UserMatchTypeEnum.EMAIL);

    verify(userService).getUser(anyString(), any(UserMatchTypeEnum.class));

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void create_user_successfully() {
    UserDto expected = DataLoader.loadFile("data/user/createUserResponse.json", UserDto.class, true);
    expected.setUserId(UUID.randomUUID());

    UserDto request = DataLoader.loadFile("data/user/createUserRequest.json", UserDto.class, true);

    when(userService.createUser(any(UserDto.class))).thenReturn(expected);

    UserDto actual = demoService.createUser(request);

    verify(userService).createUser(any(UserDto.class));

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void update_successfully() {
    UserDto expected = DataLoader.loadFile("data/user/updateUserResponse.json", UserDto.class, true);
    UpdateUserDto request = DataLoader.loadFile("data/user/updateUserRequest.json", UpdateUserDto.class, true);

    when(userService.updateUser(eq("testUser"), any(UpdateUserDto.class)))
        .thenReturn(expected);

    UserDto actual = demoService.updateUser("testUser", request);

    verify(userService).updateUser(eq("testUser"), any(UpdateUserDto.class));

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void get_session_successfully() {
    SessionDto expected = DataLoader.loadFile("data/session/sessionResponse.json", SessionDto.class, true);

    when(sessionService.getSession(any(LocalDate.class))).thenReturn(expected);

    SessionDto actual = demoService.getSession(LocalDate.of(2021, 1, 1));

    verify(sessionService).getSession(any(LocalDate.class));

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void get_sessions_by_date_successfully() {
    List<SessionDto> expected = DataLoader.loadFile("data/session/sessionsResponse.json", new TypeToken<List<SessionDto>>(){}.getType());

    when(sessionService.getSessions(any(LocalDate.class), anyInt(), anyInt())).thenReturn(expected);

    List<SessionDto> actual = demoService.getSessions(LocalDate.of(2021, 1, 1), 0, 0);

    verify(sessionService).getSessions(any(LocalDate.class), anyInt(), anyInt());

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void create_session_successfully() {
    SessionDto request = DataLoader.loadFile("data/session/createSessionRequest.json", SessionDto.class, true);

    SessionDto expected = DataLoader.loadFile("data/session/createSessionRequest.json", SessionDto.class, true);
    expected.setId(UUID.randomUUID());

    when(userService.getUserDaoByUsername(eq("testUser"))).thenReturn(Optional.of(DataLoader.loadFile("data/user/testUserDao1.json", UserDao.class)));
    when(userService.getUserDaoByUsername(eq("testUser2"))).thenReturn(Optional.of(DataLoader.loadFile("data/user/testUserDao2.json", UserDao.class)));
    when(userService.getUserDaoByUsername(eq("testUser3"))).thenReturn(Optional.of(DataLoader.loadFile("data/user/testUserDao3.json", UserDao.class)));
    when(sessionService.createSession(anyList(), any(SessionDto.class))).thenReturn(expected);

    SessionDto actual = demoService.createSession(request);

    verify(userService, times(3)).getUserDaoByUsername(anyString());
    verify(sessionService).createSession(anyList(), any(SessionDto.class));

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void delete_session_successfully() {
    doNothing().when(sessionService).deleteSession(any(UUID.class));

    assertDoesNotThrow(() -> demoService.deleteSession(UUID.randomUUID()));

    verify(sessionService).deleteSession(any(UUID.class));
  }
}
