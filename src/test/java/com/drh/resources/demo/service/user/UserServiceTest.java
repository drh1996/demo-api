package com.drh.resources.demo.service.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.drh.resources.demo.exception.UserNotFoundException;
import com.drh.resources.demo.model.UserMatchTypeEnum;
import com.drh.resources.demo.persistance.dao.UserDao;
import com.drh.resources.demo.persistance.dto.UserDto;
import com.drh.resources.demo.persistance.repository.UserRepository;
import com.drh.resources.demo.persistance.repository.UserRoleRepository;
import com.drh.resources.demo.support.DataLoader;
import com.drh.resources.demo.support.MockitoUnitTest;
import java.util.Map;
import java.util.Optional;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;

@DisplayName("User Service Unit Tests")
public class UserServiceTest extends MockitoUnitTest {

  @Mock
  private UserRepository userRepository;

  @Mock
  private UserRoleRepository userRoleRepository;

  @Mock
  private ModelMapper modelMapper;

  private UserService userService;

  @BeforeEach
  public void beforeEach() {
    userService = new UserServiceImpl(userRepository, userRoleRepository, modelMapper);
  }

  @AfterEach
  public void afterEach() {
    verifyNoMoreInteractions(userRepository, modelMapper);
  }

  @Test
  public void get_user_by_username_successfully() {
    UserDto expected = DataLoader.loadFile("data/user/userDtoResponse.json", UserDto.class, true);

    when(userRepository.findByUsername(anyString()))
        .thenReturn(Optional.of(DataLoader.loadFile("data/user/testUserDao1.json", UserDao.class)));
    when(modelMapper.map(any(UserDao.class), eq(UserDto.class))).thenReturn(expected);

    UserDto actual = userService.getUser("testUser", UserMatchTypeEnum.USERNAME);

    verify(userRepository).findByUsername(anyString());
    verify(modelMapper).map(any(UserDao.class), eq(UserDto.class));

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void get_user_by_email_successfully() {
    UserDto expected = DataLoader.loadFile("data/user/userDtoResponse.json", UserDto.class, true);

    when(userRepository.findByEmail(anyString()))
        .thenReturn(Optional.of(DataLoader.loadFile("data/user/testUserDao1.json", UserDao.class)));
    when(modelMapper.map(any(UserDao.class), eq(UserDto.class))).thenReturn(expected);

    UserDto actual = userService.getUser("test@email.com", UserMatchTypeEnum.EMAIL);

    verify(userRepository).findByEmail(anyString());
    verify(modelMapper).map(any(UserDao.class), eq(UserDto.class));

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void get_user_not_found() {
    UserNotFoundException expected = new UserNotFoundException(getMessage("error.user.not-found"),
        Map.of("identifier", "test@email.com",
            "matchType", "email"));

    when(userRepository.findByEmail(anyString()))
        .thenReturn(Optional.empty());

    UserNotFoundException actual = assertThrows(UserNotFoundException.class, () -> userService.getUser("test@email.com", UserMatchTypeEnum.EMAIL));

    verify(userRepository).findByEmail(anyString());

    assertThat(actual)
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void get_user_dao_by_username_successfully() {
    UserDao expected = DataLoader.loadFile("data/user/testUserDao1.json", UserDao.class);

    when(userRepository.findByUsername(anyString())).thenReturn(Optional.of(expected));

    Optional<UserDao> actual = userService.getUserDaoByUsername("testUser");

    verify(userRepository).findByUsername(anyString());

    assertThat(actual.isPresent()).isTrue();

    assertThat(actual.get())
        .usingRecursiveComparison()
        .isEqualTo(expected);
  }

  @Test
  public void get_user_dao_by_username_not_found() {
    when(userRepository.findByUsername(anyString())).thenReturn(Optional.empty());

    Optional<UserDao> actual = userService.getUserDaoByUsername("testUser");

    verify(userRepository).findByUsername(anyString());

    assertThat(actual.isPresent()).isFalse();
  }

  @Test
  public void create_user_successfully() {

  }

  @Test
  public void create_user_email_unavailable() {

  }

  @Test
  public void create_user_username_unavailable() {

  }

  @Test
  public void update_user_successfully() {

  }

  @Test
  public void update_user_not_found() {

  }

  @Test
  public void update_user_username_unavailable() {

  }

  @Test
  public void update_user_email_unavailable() {

  }
}
